<?php
/**
 * Search results page
 * 
 * Please see /external/starkers-utilities.php for info on HKSN_Utilities::get_template_parts()
 * Filter relies on Relevanssi plugin
 *
 * @package   WordPress
 * @subpackage  HKSN Base Template Kit
 */
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

<?php //hksn_breadcrumbs(); ?>

<h1>Search Results for '<?php echo get_search_query(); ?></h1>
<h3>Filter results or <a href="#" data-reveal-id="searchModal" title="Search">start a new search</a></h3>
<?php 
// -----------------------------------------------------------------------
// Filter ----------------------------------------------------
// Requires Relevanssi plugin

// Get URL parameters for filters
$url            = get_bloginfo('url');
    
if( isset( $_GET["region"] ) ) {
  $region_id          = $_GET["region"];

  $this_region        = get_term( $region_id, 'region' );
  $this_region_name   = $this_region->name;
}

  $url_post       = esc_attr(add_query_arg(array('post_types' => 'post', 's' => get_search_query())));
  $url_event      = esc_attr(add_query_arg(array('post_types' => 'event', 's' => get_search_query())));
  //$clear_url      = esc_attr(remove_query_arg( 'post_types' ));
  //$clear_url      = esc_attr(remove_query_arg( 'region' ));
  $url_params     = array( 'post_types', 'region');
  $clear_url      = esc_url( remove_query_arg( $url_params ) );
  $url_region     = $url.$clear_url;

?>
<?php //<label for="category">Region:</label> ?>
<select id="rlv_cat" name="rlv_cat">

<?php if( isset( $_GET["region"] ) ) { ?>
  <option value="<?php echo $region_id; ?>" selected><?php echo $this_region_name; ?></a></option>
<?php } else { ?>
  <option value="">All regions</option>
<?php } ?>
<?php $regions = get_categories('taxonomy=region'); ?>
<?php foreach ($regions as $region) : ?>
  <option value="<?php echo $region->cat_ID; ?>"><?php echo $region->name; ?></a></option>
<?php endforeach; ?>
<?php //<option value="388">Midlands</option> ?>
</select>

<script>
  <!--
  var dropdown = document.getElementById("rlv_cat");
  function onCatChange() {
      if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
          location.href = "<?php echo $url_region; ?>"+"&region="+dropdown.options[dropdown.selectedIndex].value;
      }
  }
  dropdown.onchange = onCatChange;
  -->
</script>

<div class="row">
<?php        
    echo "
    <div class='large-4 columns'>
        <a href='$url_post' rel='nofollow' class='button expanded'>Filter by Posts</a>
    </div>";
    echo "
    <div class='large-4 columns'>
        <a href='$url_event' rel='nofollow' class='button expanded'>Filter by Events</a>
    </div>";
    echo "
    <div class='large-4 columns'>
        <a href='$clear_url' rel='nofollow' class='button textonly'>Clear filters</a>
    </div>";
?>
</div>
<?php /* 
<div class="large-2 right columns">
<input id="member-search-submit" type="submit" value="search" class="button expanded"> 
</div>*/ ?>
<?php 
// -----------------------------------------------------------------------
// Results ----------------------------------------------------
// 
?>
<?php if ( have_posts() ): ?>
<div class="row">
  <?php while ( have_posts() ) : the_post(); ?>

    <?php HKSN_Utilities::get_template_parts( array( 'inc/modules/loop_search-results' ) ); ?>

  <?php endwhile; ?>
</div>
<?php else: ?>
<h3>No results found</h3>
<br>
<h4>Start a new search</h4>
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url('/'); ?>">
  <div class="row">
    <div class="medium-9 columns">
        <label class="screen-reader-text" for="s">Search for:</label>
        <input value="" name="s" id="s" type="text" placeholder="Keyword">
    </div>
    <div class="medium-3 columns">
        <button class="searchsubmit">Search</button>
    </div>
  </div>
</form>
<?php endif; ?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/footer','inc/shared/html-footer' ) ); ?>