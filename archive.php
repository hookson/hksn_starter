<?php
/**
 * The template for displaying Archive post pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on HKSN_Utilities::get_template_parts() 
 *
 * @package   WordPress
 * @subpackage  HKSN Base Template Kit
 */
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

        <!-- Main content -->
        <main role="main" id="maincontent">
            <div class="section">



                <div class="row">
                    <div class="small-12 columns">
                        <div class="primary widget-intro left-aligned">
                            <h1 class="page-title">News Archive</h1>
        
                            <ul class="filter">
                                <?php wp_list_categories( array(
                                    'orderby'    => 'name',
                                    'title_li'   => '',
                                    'depth'      => 1,
                                    'exclude'    => array( 12 )
                                  ) ); ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="large-8 xlarge-9 columns"> 
<?php if ( have_posts() ): ?>
    <?php while ( have_posts() ) : the_post(); ?>
                        <?php
                        // -----------------------------------------------------------------------
                        // Output all posts
                        // 
                        ?>
                        <?php 
                            // Post query loop     
                            $posts_per_row      = 3; 
                            include( locate_template( 'inc/modules/post/loop.php' ) ); 
                        ?>
    <?php endwhile; // End post loop ?>
<?php else: ?>
                        <div class="general-content">
                            <p>No posts to display</p>
                        </div>
<?php endif; ?>
                    </div>
        
                    <div class="large-4 xlarge-3 columns sidebar">
                        <?php HKSN_Utilities::get_template_parts( array( 'inc/shared/sidebar' ) ); ?>
                    </div>
                </div>

            </div>
        </main>
        <!-- End main content -->

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/footer','inc/shared/html-footer' ) ); ?>