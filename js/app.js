jQuery(document).ready(function($) {
  $(document).foundation({//'reveal',{ animation: 'none' }
      abide: {
        patterns: {
          password: /^(.){8,}$/
        },
        validators: {
              checkbox_limit: function(el, required, parent) {
                  var group = parent.closest('.checkbox-group');
                  var min = group.attr('data-abide-validator-min');
                  var checked = group.find(':checked').length;
                  if (checked >= min) {
                      group.find('small.error').hide();
                      return true;
                  } else {
                      group.find('small.error').css({
                          display: 'block'
                      });
                      return false;
                  }
              }
          }
      }
  });
  // Initialise Cookie Notice -------------------------------------------------------------------------------
  //$('#cookie-notice').cookieNote();


  //------------------
  // Desktop navigation (show submenus)
  //------------------
  $('.dropdown .has-dropdown').append('<span class="show-for-xxlarge dropdown-btn-toggle"></span>');

  if ($(window).innerWidth() >= 1200) {
    var closeSubmenu = function() {
        $('.dropdown .dropdown-btn-toggle').parent().removeClass('active').children('.dropdown').removeClass('show');
    };
    closeSubmenu();

    $('.dropdown .dropdown-btn-toggle').on('click', function() {
      var $parent = $(this).parent();

        // $(this).removeClass('active').find('.dropdown').removeClass('show');

        if (!$parent.hasClass('active')) {
          $(this).parent().toggleClass('active').children('.dropdown').toggleClass('show');
        } else {
          $('.dropdown .dropdown-btn-toggle').parent().removeClass('active').children('.dropdown').removeClass('show');
        }
    });
  }

  $('.w-expandable .more').click(function(e) {
    //console.log( $(this).prev('div') );
      //e.preventDefault();
      console.log( $(this).closest('.w-expandable').height() );

      //var wContentH = $(this).height();
      $(this).closest('.w-expandable').removeAttr('style');
      //alert( $(this).closest('.widget-profile').html() );
      $(this).prev('div').toggleClass('open');
      $(this).toggleClass('open');
      $.fn.matchHeight._update();

      console.log( $(this).closest('.w-expandable').height() );
  });

  /* -------------------------------------------------------------------------------
  function setupBios() {

      $('.widget-profile .hide-bio').hide();

       $('.widget-profile .show-bio').on('click', function () {
          var $widget = $(this).closest('.widget-profile');
          $(this).closest('.w-body').removeAttr('style');
          $widget.find('.bio-content').show();
          $.fn.matchHeight._update();
          $widget.find('.bio-content').hide();
          $widget.find('.bio-content').slideDown( "fast", function() {});
          $widget.find('.hide-bio').show();
           $widget.find('.show-bio').hide();

      });
      $('.widget-profile .hide-bio').on('click', function () {
          var $widget = $(this).closest('.widget-profile');
          $widget.find('.bio-content').slideUp( "fast", function() {
               $.fn.matchHeight._update();
            });
          $widget.find('.hide-bio').hide();
           $widget.find('.show-bio').show();

      });
  }*/
  // Full width mega/sub-menu -------------------------------------------------------------------------------
  // $(".megamenu").mouseenter(function(){
  //   if($(window).width()>1024){
  //     $(".dropdown-wrapper").offset({left: 0});
  //     $(".dropdown-wrapper").css("width",$(window).width());
  //   }else{
  //     $(".dropdown-wrapper").css("width",auto);
  //   }
  // });

  // Sign-up bar slide down (with delay) -------------------------------------------------------------------------------
  $(".signup-bar").slideUp(1).delay(1000).slideDown('slow');
  $('.signup-bar .fa-close').click(function(e) {
      e.preventDefault();
      $(this).parent().slideUp('slow');
  });

  // Search panel -------------------------------------------------------------------------------
  // searchlink = $('#s-trigger');
  // searchsubmit = $('#searchsubmit');
  // searchcontainer = $('.search-frame');
  // searchvalue = $('#s');
  // searchform = $('#searchform');
  // featuredbar = $('#featuredbar');

  // searchlink.click(function(e) {
  //     e.preventDefault();
  //     searchcontainer.addClass('open');
  //     featuredbar.addClass('receed');
  //     return false;
  // });

  // searchsubmit.click(function(e) {
  //     e.preventDefault();
  //     var text = searchvalue.val();
  //     if(text){
  //       searchform.submit();
  //     }
  //     searchcontainer.removeClass('open');
  //     featuredbar.removeClass('receed');
  //     //return false;
  // });

  // Match grid panel heights -------------------------------------------------------------------------------
  if($(window).width() > 481) {
    //$('.widget-note.view-mode-search_result .widget-container').matchHeight();
    $('.widget-portrait.mh .w-body').matchHeight();
    $('.widget-option').matchHeight();
    $('.widget-option .section-title').matchHeight();
    $('.widget-option .w-body').matchHeight();
  }
  if($(window).width() > 768) {
    $('.columns.match-height').matchHeight();
    $('.widget-text.match-height').matchHeight();
    $('.widget-profile.w-simple').matchHeight();
    $('.listing-summary').matchHeight();
  }

  // Enhacement for browsers which don't support media queries (IE8)
  if (respond && !respond.mediaQueriesSupported) {
      var delayedUpdate = function() {
          setTimeout($.fn.matchHeight._update, 300);
      };

      $(delayedUpdate);
      $(window).bind('load resize orientationchange', delayedUpdate);
  }

  // Reveals --------------------------------------------------------------------------------------------
  $('#filter-topic ul').hide();
  $('#filter-location ul').hide();

  /*$('#filter-topic h4').click(function(){
    $("#filter-topic ul").show();
  });*/

  $('#filter-bar h4').click(function() {

    $(this).toggleClass("open");

    $(this).next('ul').slideToggle('slow', function() {
    // Animation complete.
      //$(this).prev().toggleClass("open");
    });
    return false;
  });

  // ---------------------------------------------------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------------------
  // Form scripts --------------------------------------------------------------------------------------------

  // Clear calculation fields on refresh ---------------------------------------------------------------------
  $('#discount_code').val('');
  $('#additional_donation').val('');

  // Form toggles and controls -------------------------------------------------------------------------------
  //$('#gift_fields').hide(); use PHP to hide/show on load
  //$('#gift_mail_fields').hide();
  //$('#honoreegift_fields').hide();
  //$('#additional_member_fields').hide();
  $('#ticket_contribution').hide();
  $("#payment-form.inactive").hide(); // Enabled in function switchMembership(name,price) {}

  $('#giftmembership').change(function() {
      $('#gift_fields').toggle(this.checked);
  });
  $('#samebilling').change(function() {
      $('#billing_address_fields').toggle(!this.checked);
  });

  $('input[name="gift_mail"]').click(function(){
    if($(this).attr("value")=="Mail to me"){
        $("#gift_mail_fields").show();
    }
    if($(this).attr("value")=="Mail to recipient"){
        $("#gift_mail_fields").hide();
    }
  });

  $('#honoreegift').change(function() {
    $('#honoreegift_fields').toggle(this.checked);
  });

  $('input[name="chosen_ticket"]').click(function(){
    if($(this).attr("value")=="Contribution"){
        $("#ticket_contribution").show();
    }
    if($(this).attr("value")!="Contribution"){
        $("#ticket_contribution").hide();
    }
  });

  // Choose new membership type -------------------------------------------------------------------------------
  $('.membtype_open').click(function(){
    $("#membership_types_chosen").hide();
    $("#payment-form").hide();
    $("#membership_types").show();
  });

  // Calculate total with donation -------------------------------------------------------------------------------
  // Use jQuery 'keyup' to trigger the computation as the user types
  $('#additional_donation').keyup(function () {

      // initialize the sum (donation) to zero
      var donation = 0;

      // get the current total (totalcost)
      var totalcost = parseInt(jQuery('#total_amount').attr('data-total'));

      // we use jQuery each() to loop through all the textbox with 'price' class
      // and compute the sum for each loop
      $('#additional_donation').each(function() {
          totalcost += Number($(this).val());
          donation += Number($(this).val());
      });

      // Set the computed value to 'total_amount' summary
      $('#total_amount').text(totalcost.toFixed(2));

      // Set donation attribute (in case the user changes membership type)
      $('#total_amount').attr('data-donation',donation);
  });

  // Form textarea calculations -------------------------------------------------------------------------------
  // Use jQuery 'keyup' to trigger the input total as the user types
  $('#gift_message').keyup(function(){
      $('#gift_message_count').text('100 characters left');
      var max = 100;
      var len = $(this).val().length;
    if (len >= max) {
       $('#gift_message_count').text('You have reached the limit, please delete some text');
    } else {
        var ch = max - len;
        $('#gift_message_count').text(ch + ' characters left');
    }
  });
  $('#honoree_gift_message').keyup(function(){
      $('#honoree_gift_message_count').text('100 characters left');
      var max = 100;
      var len = $(this).val().length;
    if (len >= max) {
       $('#honoree_gift_message_count').text('You have reached the limit, please delete some text');
    } else {
        var ch = max - len;
        $('#honoree_gift_message_count').text(ch + ' characters left');
    }
  });

  $('#special_instructions').keyup(function(){
      $('#special_instructions_count').text('40 characters left');
      var max = 40;
      var len = $(this).val().length;
    if (len >= max) {
       $('#special_instructions_count').text('You have reached the limit, please delete some text');
    } else {
        var ch = max - len;
        $('#special_instructions_count').text(ch + ' characters left');
    }
  });


  //---------------------------------
  //---------------------------------
  // Image map
  //---------------------------------
  // Highlight map regions and show their descriptions
  $('map area').each(function (idx, elem) {
      $('#area' + (++idx)).on('hover', function() {
        $('#map_highlight').removeClass();
        $(this).closest('.map_widget').find('#map_highlight').addClass('area' + (idx - 1));
      }), function() {
        $('#map_highlight').removeClass();
      };

      $('#area' + (++idx)).on('click', function(e) {
        e.preventDefault();
        $('#map_highlight').removeClass().addClass('area' + idx);
        $('.area-description .area-item').removeClass('show');
        $('.area-description .area-item.area' + idx).addClass('show');

        $('.area-description .area-item .close-area-item').on('click', function() {
          if ($(this).parent().hasClass('show')) {
            $(this).parent().removeClass('show').closest('.map_widget').find('#map_highlight').removeClass();
          }
        });
      });
    });


  //---------------------------------
  //---------------------------------
  // Forms
  //---------------------------------
  // Select multiple values without pressing ctrl in a select multiple element
  $("select[multiple]").mousedown(function(e){
    e.preventDefault();

    var select = this;
    var scroll = select.scrollTop;

    e.target.selected = !e.target.selected;

    setTimeout(function(){select.scrollTop = scroll;}, 0);

    $(select).focus();
  }).mousemove(function(e){e.preventDefault()});

  // Filtering form & pagination (news & blog posts)
  var query_string_params = {};
  window.location.search && $.each(window.location.search.substring(1).split('&'), function (key, val) {
    var values = val.split('=');
    query_string_params[values[0]] = values[1];
  });

  var get_query_string_params_string = function () {
    var str = '';
    $.each(query_string_params, function (key, value) {
      str += (str !== '' ? '&' : '') + key + '=' + value;
    });
    return '?' + str;
  };

  $(".filter-by-category").on('change', function () {
    var category_id = $(this).find('option:selected').val();
    var path = window.location.pathname;
    query_string_params['category_id'] = category_id;
    if (query_string_params.page_no) {
      delete query_string_params.page_no;
    }
    if (category_id === 'all') {
      delete query_string_params.category_id;
    }

    window.location.href = path + get_query_string_params_string();
  });

  // Pagination
  var changePage = function (type) {
    if (!type) {
      type = 1;
    }
    return function () {
      var page = $(this).data('page');
      var path = window.location.pathname;
      query_string_params['page_no'] = page + 1 * type;
      window.location.href = path + get_query_string_params_string();
    };
  };

  $(".prev-page").on('click', changePage(-1));
  $(".next-page").on('click', changePage(1));

  

  // Display and remove overlay menu -------------------------------------------------------------------------------

    // Template # 3 - Modal
    /*
    $('#trigger-overlay').click(function() {
      $('.overlay.menu-overlay').addClass('open');
    });
    $('.overlay-close').click(function() {
      $('.overlay.menu-overlay').removeClass('open');
    });*/

    // Template # 2 - Off-canvas
    // $('#trigger-overlay').click(function() {
    //     $('.overlay.menu-overlay').toggleClass('open');
    //     $('body').toggleClass('no-scroll');
    //     $('#body-overlay').toggleClass('hidden');
    //     $('.main-content-wrapper').toggleClass('move');
    //     $('#fixed-quick-bar').toggleClass('move');
    // }) ;
    //
    // $('.action-close').click(function() {
    //     $('.overlay.menu-overlay').removeClass('open');
    //     $('body').removeClass('no-scroll');
    //     $('#body-overlay').addClass('hidden');
    //     $('.main-content-wrapper').toggleClass('move');
    //     $('#fixed-quick-bar').toggleClass('move');
    // }) ;
    //
    // $('#body-overlay').click(function() {
    //     $('.overlay.menu-overlay').removeClass('open');
    //     $('body').removeClass('no-scroll');
    //     $('#body-overlay').addClass('hidden');
    //     $('.main-content-wrapper').toggleClass('move');
    //     $('#fixed-quick-bar').toggleClass('move');
    // }) ;

    // Reveal blocks -------------------------------------------------------------------------------
    /*$(".w-expandable div.w-content").mouseenter(function(){
      if($(window).width()>641){

      }
    }*/

  // Sticky DIV -------------------------------------------------------------------------------
  // var scrollElement = $('#stickyBlock');

  // if($(window).width()>1025 && scrollElement.length) {
  //   $(window).scroll(function () {

  //       var scroll = $(this).scrollTop();
  //       var scrollheight = $('#stickyBlock').height() + 'px';
  //       var baseheight = $('#stickyBase').height() + 'px';

  //       if (scroll < $('#payment-form').offset().top) {

  //           $('#stickyBlock').css({
  //               'position': 'absolute',
  //               'top': '60px'
  //           });

  //       } else if (scroll > $('#stickyBase').offset().top) {

  //           $('#stickyBlock').css({
  //               'position': 'absolute',
  //               'bottom': '470px', // Must match #stickyBlock height in app.css
  //               'top': 'auto'
  //           });

  //       } else {

  //           $('#stickyBlock').css({
  //               'position': 'fixed',
  //               'top': '60px',
  //               'height': scrollheight
  //           });
  //       }
  //   });
  // }

  // Slick Carousel -------------------------------------------------------------------------------
  // Note: All hero carousels are disabled as not needed - if enabled check output FIRST
    // Hero Carousel
    /*$('.hero-carousel').slick({
        dots: true,
        infinite: true,
        speed: 800,
        autoplay: false
    });*/

    // $('.slider').slick({
    //     dots: false,
    //     slidesToShow: 2,
    //     prevArrow: '<img src="img/arrow-left-white.png" class="fa fa-angle-left" />',
    //     nextArrow: '<img src="img/arrow-right-white.png" class="fa fa-angle-right" />',
    //     infinite: true,
    //     speed: 300,
    //     autoplay: false,
    //     responsive: [
    //         {
    //           breakpoint: 1024,
    //           settings: {
    //             slidesToShow: 2,
    //             slidesToScroll: 2
    //           }
    //         },
    //         {
    //           breakpoint: 1000,
    //           settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //           }
    //         }
    //     ]
    // });


    // // Facts Carousel
    // $('.snippet-carousel').slick({
    //     dots: false,
    //     prevArrow: '<i class="fa fa-angle-left"></i>',
    //     nextArrow: '<i class="fa fa-angle-right"></i>',
    //     infinite: true,
    //     speed: 300,
    //     autoplay: false,

    // });
    // // Gallery Carousel
    // $('.gallery-carousel').slick({
    //     dots: false,
    //     prevArrow: '<i class="fa fa-angle-left"></i>',
    //     nextArrow: '<i class="fa fa-angle-right"></i>',
    //     infinite: true,
    //     speed: 300,
    //     autoplay: false,
    //     slidesToShow: 3
    // });
    // $('.gallery-slide-container').slick({
    //     dots: false,
    //     prevArrow: '<img src="img/arrow-black-gallery-left.png" class="left" alt="move-left"/>',
    //     nextArrow: '<img src="img/arrow-black-gallery.png" class="right" alt="move-right"/>',
    //     infinite: true,
    //     speed: 600,
    //     autoplay: false,
    //     slidesToShow: 3,
    //     slidesToScroll: 3,
    //     responsive: [{
    //       breakpoint: 1024,
    //         settings: {
    //           slidesToShow: 2,
    //           slidesToScroll: 2
    //         }
    //       },
    //       {
    //         breakpoint: 560,
    //         settings: {
    //           slidesToShow: 1,
    //           slidesToScroll: 1
    //         }
    //       }
    //     ]
    // });
    // $('.gallery-slide-container__alt').slick({
    //     dots: false,
    //     prevArrow: '<img src="img/arrow-left-white.png" class="left" alt="move-left"/>',
    //     nextArrow: '<img src="img/arrow-right-white.png" class="right" alt="move-right"/>',
    //     infinite: true,
    //     speed: 600,
    //     autoplay: false,
    //     slidesToShow: 2,
    //     slidesToScroll: 2,
    //     responsive: [
    //       {
    //         breakpoint: 1270,
    //         settings: {
    //           slidesToShow: 1,
    //           slidesToScroll: 1,
    //       }
    //     }
    //     ]
    // });

    // $('.center-carousel').slick({
    //   dots: false,
    //   slidesToShow: 4,
    //   slidesToScroll: 4,
    //   prevArrow: '<img src="img/arrow-left.png" class="fa fa-angle-left" />',
    //   nextArrow: '<img src="img/arrow-right.png" class="fa fa-angle-right" />',
    //   infinite: true,
    //   speed: 300,
    //   autoplay: false,
    //   responsive: [
    //       {
    //         breakpoint: 1224,
    //         settings: {
    //           slidesToShow: 3,
    //           slidesToScroll: 3,
    //           infinite: true,
    //           dots: true
    //         }
    //       },
    //       {
    //         breakpoint: 900,
    //         settings: {
    //           slidesToShow: 2,
    //           slidesToScroll: 2
    //         }
    //       },
    //       {
    //         breakpoint: 480,
    //         settings: {
    //           slidesToShow: 1,
    //           slidesToScroll: 1
    //         }
    //       }
    //   ]
    // });
    // // Tabbed Carousel
    // $('.tabbed-carousel').slick({
    //     dots: true,
    //     prevArrow: '<i class="fa fa-angle-left"></i>',
    //     nextArrow: '<i class="fa fa-angle-right"></i>',
    //     infinite: true,
    //     speed: 300,
    //     autoplay: false,
    //     customPaging: function(slick,index) {
    //       var tab = $(slick.$slides[index]).data('tab');
    //       return '<a>'+tab+'</a>';
    //     }
    // });

});
