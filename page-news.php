<?php
/**
 * Template name: News
 * Used by: Latest... 
 *
 * @package 	WordPress
 * @subpackage 	HKSN Base Template Kit
 */
?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
// -----------------------------------------------------------------------
// Introduction ----------------------------------------------------
// Default page content (title and content)
// If used (sometimes excluded from front page)
?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>

<?php endwhile; // End post loop ?>

<?php
// -----------------------------------------------------------------------
// Featured Posts ----------------------------------------------------
// Output all posts with featured category
//
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/modules/featured_posts' ) ); ?>

<?php
// -----------------------------------------------------------------------
// All Posts plus filter ----------------------------------------------------
// Output all posts 
//
?>
<div class="row">
	<div class="large-4 columns">
		<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/sidebar-filter' ) ); ?>
	</div>
	<div class="large-8 columns">
		<?php HKSN_Utilities::get_template_parts( array( 'inc/modules/all_posts' ) ); ?>
	</div>
</div>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/footer','inc/shared/html-footer' ) ); ?>