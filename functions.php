<?php
	/**
	 * HKSN functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 */

	/* ========================================================================================================================

	Required external files

	======================================================================================================================== */

	require_once( 'external/utilities.php' );

	// Register custom post types
	require_once('external/custom_post_types.php');

	// Register custom menus
	require_once('external/menu.php');

	// Create breadcrumbs
	require_once('external/breadcrumbs.php');

	/* ========================================================================================================================

	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme

	======================================================================================================================== */

	add_theme_support('post-thumbnails');
  	//set_post_thumbnail_size( 480, 250, true );

  	//add_image_size( 'Profile', 260, 260, true );
  	add_image_size( 'HeroMain', 1600, 450, true );
  	add_image_size( 'HeroPage', 1600, 360, true );
  	add_image_size( 'Teaser', 580, 580, true );
  	add_image_size( 'ArticleImage', 480, 280, true );
  	add_image_size( 'ArticleImagePortait', 280, 480, true );
  	add_image_size( 'Gallery', 900, 525, true );
  	add_image_size( 'TeaserSmall', 280, 280, true );

  	// Add custom sizes to Media Uploader
	add_filter('image_size_names_choose', 'hksn_image_sizes');

	function hksn_image_sizes($sizes) {
		$addsizes = array(
			"ArticleImage" => __( "Default Article Image"),
			"ArticleImagePortait" => __( "Portrait Article Image")
		);
		$newsizes = array_merge($sizes, $addsizes);
		return $newsizes;
	}

  	// Hide Admin bar
  	add_filter('show_admin_bar', '__return_false');

  	// Add function to detect all children in page tree (https://css-tricks.com/snippets/wordpress/if-page-is-parent-or-child/)
  	function is_tree($pid) {								// $pid = The ID of the page we're looking for pages underneath
		global $post;         								// load details about this page
		if(is_page()&&($post->post_parent==$pid||is_page($pid)))
	        return true;   									// we're at the page or at a sub page
		else
	        return false;  									// we're elsewhere
	};

	//register_nav_menus(array('primary' => 'Primary Navigation'));
	//register_nav_menus(array('secondary' => 'Secondary Navigation')); // Right-hand links e.g. 'My Church'

	/* ========================================================================================================================

	Actions and Filters

	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'hksn_script_enqueuer' );

	add_filter( 'body_class', array( 'HKSN_Utilities', 'add_slug_to_body_class' ) );

	/* ========================================================================================================================

	Post pagination

	======================================================================================================================== */

	// Search page results
	function change_wp_search_size($query) {
	    if ( $query->is_search ) // Make sure it is a search page
	        $query->query_vars['posts_per_page'] = -1; // Change 10 to the number of posts you would like to show

	    return $query; // Return our modified query variables
	}
	add_filter('pre_get_posts', 'change_wp_search_size'); // Hook our custom function onto the request filter

	/* ========================================================================================================================

	Advanced Custom Fields

	======================================================================================================================== */
	function my_acf_init() {

		acf_update_setting('google_api_key', 'AIzaSyACIShEoSlIAFkaKjmv5aC-H6Lqp-IzIpA');
	}

	add_action('acf/init', 'my_acf_init');

	/* ========================================================================================================================

	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );

	======================================================================================================================== */

	// Custom archive/loops: change number of posts to show in custom posts
	add_action('pre_get_posts','show_all_events');

	function show_all_events( $query ) {
	  if ( $query->is_main_query() && is_post_type_archive('events') ) {
	    $query->set('posts_per_page', -1);
	  }
	}
	/* ========================================================================================================================

	Admin - Move to separate external PHP include

	======================================================================================================================== */


	// Custom Styles: Callback function to insert 'styleselect' into the $buttons array
	function hksn_mce_buttons_2( $buttons ) {
	    array_unshift( $buttons, 'styleselect' );
	    return $buttons;
	}
	// Register our callback to the appropriate filter
	add_filter('mce_buttons_2', 'hksn_mce_buttons_2');

	// Custom Styles: Callback function to filter the MCE settings
	function hksn_mce_before_init_insert_formats( $init_array ) {
	    // Define the style_formats array
	    $style_formats = array(
	        // Each array child is a format with it's own settings
	        array(
	            'title' => 'Button Link',
	            'selector' => 'a',
	            'classes' => 'button'
	        ),
	        array(
	            'title' => 'Block List',
	            'selector' => 'ul',
	            'classes' => 'block-list'
	        ),
	        array(
	        	'title' => 'Image Wrapper',
	        	'block' => 'div',
	        	'classes' => 'image-wrapper',
	        	'wrapper' => true
	        ),
	        array(
	        	'title' => 'Footnote',
	        	'selector' => 'p',
	        	'classes' => 'footnote'
	        ),
	        array(
	        	'title' => 'Section Title',
	        	'block' => 'div',
	        	'classes' => 'section-title-wrapper',
	        	'wrapper' => true
	        	/*'items' => array(
		            array(
		                'title' => 'Heading 1',
		                'selector' => 'h1',
		                'classes' => 'section-title'
		            ),
		            array(
		                'title' => 'Heading 2',
		                'selector' => 'h2',
		                'classes' => 'section-title'
		            ),
		            array(
		                'title' => 'Heading 3',
		                'selector' => 'h3',
		                'classes' => 'section-title'
		            ),
		            array(
		                'title' => 'Heading 4',
		                'selector' => 'h4',
		                'classes' => 'section-title'
		            ),
		        )*/
	        ),
	    );
	    // Insert the array, JSON ENCODED, into 'style_formats'
	    $init_array['style_formats'] = json_encode( $style_formats );

	    return $init_array;

	}
	// Attach callback to 'tiny_mce_before_init'
	add_filter( 'tiny_mce_before_init', 'hksn_mce_before_init_insert_formats' );

	// Add custom styles to admin css
	function hksn_add_editor_styles() {
	    add_editor_style( 'css/custom-editor.css' );
	}
	add_action( 'admin_init', 'hksn_add_editor_styles' );

	/* ========================================================================================================================

	Scripts

	======================================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function hksn_script_enqueuer() {

	    // create array of all scripts
	    $head_scripts=array('modernizr'=>'/js/lib/modernizr.js');

	    // create array of all scripts
	    $foot_scripts=array(

	    	//'foundation'=>'/js/lib/foundation.min.js',
	    	//'foundation.abide'=>'/js/lib/foundation.abide.js',
	    	//'slick'=>'/js/lib/slick.min.js',
	    	//'matchheight'=>'/js/lib/jquery.matchHeight.js',
	    	//'cookie'=>'/js/lib/jquery.cookie.js',
	    	//'cookienote'=>'/js/lib/jquery.cookienote.js',
	    	//'matchmedia.polyfill'=>'/js/lib/matchmedia.polyfill.js',
	    	//'respond.src'=>'/js/lib/respond.src.js',
	    	//'jquery-ui'=>'/js/lib/jquery-ui.js',
	    	//'app'=>'/js/app.js'
	    	
	    );

	    foreach($head_scripts as $key=>$sc)
	    {
	       wp_register_script( $key, get_template_directory_uri() . $sc, array('jquery'), '1.0', false );
	       wp_enqueue_script( $key );
	    }
	    foreach($foot_scripts as $key=>$sc)
	    {
	       wp_register_script( $key, get_template_directory_uri() . $sc, array('jquery'), '1.0', true );
	       wp_enqueue_script( $key );
	    }

		wp_register_style( 'screen', get_template_directory_uri().'/style.css', '', '', 'screen' );
		// wp_register_style( 'slick', get_template_directory_uri().'/css/slick.css', '', '', 'screen' );
		//wp_register_style( 'app', get_template_directory_uri().'/css/app.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
        // wp_enqueue_style( 'slick' );
       	//wp_enqueue_style( 'app' );
	}

	/* ========================================================================================================================

	Comments

	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function hksn_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif;
	}

	/* ========================================================================================================================

	Relevanssi

	======================================================================================================================== */

	/**
	 * Add Category filter
	https://www.relevanssi.com/knowledge-base/category-filter-search-results-pages/
	*/
	add_filter('relevanssi_hits_filter', 'rlv_gather_categories', 99);
	function rlv_gather_categories($hits) { // Add 2nd variable (for taxon. e.g. topics or region)
		global $rlv_categories_present;
		$rlv_categories_present = array();
		foreach ( $hits[0] as $hit ) {
			$terms = get_the_terms( $hit->ID, 'region' );
			foreach ( $terms as $term ) {
				$rlv_categories_present[ $term->term_id ] = $term->name;
			}
		}

		asort( $rlv_categories_present );
		return $hits;
	}
	/**
	 * Build Category filter dropdown
	*/
	function rlv_category_dropdown() {
		global $rlv_categories_present, $wp_query;

		if (!empty($wp_query->query_vars['cat'])) {
			$url = esc_url(remove_query_arg('cat'));
			echo "<p><a href='$url'>Remove category filter</a>.</p>";
		}
		else {
			$select = "<select id='rlv_cat' name='rlv_cat'>
			<option value=''>Choose a category</option>";
			foreach ( $rlv_categories_present as $cat_id => $cat_name ) {
				$select .= "<option value='$cat_id'>$cat_name</option>";
			}
			$select .= "</select>";
			$url = esc_url(remove_query_arg('paged'));
			if (strpos($url, 'page') !== false) {
				$url = preg_replace('/page\/\d+\//', '', $url);
			}
			$select .= <<<EOH

	<script>
		<!--
		var dropdown = document.getElementById("rlv_cat");
		function onCatChange() {
			if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
				location.href = "$url"+"&cat="+dropdown.options[dropdown.selectedIndex].value;
			}
		}
		dropdown.onchange = onCatChange;
		-->
	</script>
EOH;

			echo $select;
		}
	}


	add_filter('relevanssi_hits_filter', 'search_result_types');
	function search_result_types( $hits ) {
	    global $hns_search_result_type_counts, $wp_query, $rlv_doing_this_already;

	    if (!$rlv_doing_this_already && $wp_query->query_vars['post_type'] != 'any') {
	    	$copy_query = $wp_query;
	    	$copy_query->query_vars['post_type'] = "any";
	    	$rlv_doing_this_already = true;
	    	relevanssi_do_query($copy_query);
	    	return $hits;
	    }

	    $types = array();
	    if ( ! empty( $hits ) ) {
	        foreach ( $hits[0] as $hit ) {
	            $types[$hit->post_type]++;
	        }
	    }

	    $hns_search_result_type_counts = $types;
	    return $hits;
	}

	/* ========================================================================================================================

	REST ADDITIONAL FIELDS

	======================================================================================================================== */

	add_action( 'rest_api_init', 'slug_register_person_address' );
	function slug_register_person_address() {
	   register_rest_field( 'consultant',
	       'person_address',
	       array(
	           'get_callback'    => 'slug_get_person_address',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_person_address( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}

	add_action( 'rest_api_init', 'slug_register_person_company' );
	function slug_register_person_company() {
	   register_rest_field( 'consultant',
	       'person_company',
	       array(
	           'get_callback'    => 'slug_get_person_company',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_person_company( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}


	add_action( 'rest_api_init', 'slug_register_person_position' );
	function slug_register_person_position() {
	   register_rest_field( 'consultant',
	       'person_position',
	       array(
	           'get_callback'    => 'slug_get_person_position',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_person_position( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}

	add_action( 'rest_api_init', 'slug_register_person_telephone' );
	function slug_register_person_telephone() {
	   register_rest_field( 'consultant',
	       'person_telephone',
	       array(
	           'get_callback'    => 'slug_get_person_telephone',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_person_telephone( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}

	add_action( 'rest_api_init', 'featured_image_thumbnail_url' );
	function featured_image_thumbnail_url() {
	   register_rest_field( 'consultant',
	       'featured_image_url',
	       array(
	           'get_callback'    => 'get_featured_image_url',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function get_featured_image_url( $object, $field_name, $request ) {
		$thumbnail_id = get_post_thumbnail_id($object->ID);
		return wp_get_attachment_image_src($thumbnail_id);
	}

	add_action( 'rest_api_init', 'slug_register_person_email' );
	function slug_register_person_email() {
	   register_rest_field( 'consultant',
	       'person_email',
	       array(
	           'get_callback'    => 'slug_get_person_email',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_person_email( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}


	add_action( 'rest_api_init', 'slug_register_person_mobile' );
	function slug_register_person_mobile() {
	   register_rest_field( 'consultant',
	       'person_mobile',
	       array(
	           'get_callback'    => 'slug_get_person_mobile',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_person_mobile( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}

	add_action( 'rest_api_init', 'slug_register_person_website' );
	function slug_register_person_website() {
	   register_rest_field( 'consultant',
	       'person_website',
	       array(
	           'get_callback'    => 'slug_get_person_website',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_person_website( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}


	add_action( 'rest_api_init', 'slug_register_event_start_date' );
	function slug_register_event_start_date() {
	   register_rest_field( 'event',
	       'event_start_date',
	       array(
	           'get_callback'    => 'slug_get_event_start_date',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_event_start_date( $object, $field_name, $request ) {
	   $date = get_post_meta( $object[ 'id' ], $field_name, true );
	   return date("m-d-Y", strtotime($date));
	}
	
	add_action( 'rest_api_init', 'slug_register_event_start_time' );
	function slug_register_event_start_time() {
	   register_rest_field( 'event',
	       'event_start_time',
	       array(
	           'get_callback'    => 'slug_get_event_start_time',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_event_start_time( $object, $field_name, $request ) {
	   return get_post_meta( $object[ 'id' ], $field_name, true );
	}
	
	add_action( 'rest_api_init', 'slug_register_event_location_city' );
	function slug_register_event_location_city() {
	   register_rest_field( 'event',
	       'event_location_city',
	       array(
	           'get_callback'    => 'slug_get_event_location_city',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_event_location_city( $object, $field_name, $request ) {
	    return get_post_meta( $object[ 'id' ], $field_name, true );
	}


	/*add_action( 'rest_api_init', 'slug_register_asset_downloads' );
	function slug_register_asset_downloads() {
	   register_rest_field( 'resource',
	       'asset_downloads',
	       array(
	           'get_callback'    => 'slug_get_asset_downloads',
	           'update_callback' => null,
	           'schema'          => null,
	       )
	   );
	}

	function slug_get_asset_downloads( $object, $field_name, $request ) {

	   	$asset_downloads = get_post_meta($object[ 'id' ], $field_name, true);
		if ($asset_downloads) {
		  for ($i=0; $i<$asset_downloads; $i++) {
		    $meta_key = '$field_name_'.$i.'_ad_download_filename';
		    $sub_field_value = get_post_meta($object[ 'id' ], $meta_key, true);
		  }
		}
		return $sub_field_value
	}*/


function hksn_add_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array('resource_category','resource_tags');

	// must set this to the post type you want the filter(s) displayed on
	if( $typenow == 'resource' ){

		foreach ($taxonomies as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";
				foreach ($terms as $term) {
					echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
				}
				echo "</select>";
			}
		}
	}
}
add_action( 'restrict_manage_posts', 'hksn_add_taxonomy_filters' );