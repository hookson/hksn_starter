<?php
/**
 * Template name: Site Map
 * The template for displaying all pages and posts as a sitemap.
 */
?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
// Get custom fields

?>
<?php
// ---------------------------------------------------------------------
// Hero Image ----------------------------------------------------------
// Output hero image (if exists)
?>
<?php //HKSN_Utilities::get_template_parts( array( 'inc/modules/hero_image' ) ); ?>
<div class="hero empty"></div>
<div class="breadcrumb">
  <div class="row">
    <div class="large-12 columns">
      <?php hksn_breadcrumbs(); ?>
    </div>
  </div>
</div>
<?php
// -----------------------------------------------------------------------
// Introduction ----------------------------------------------------
// Default page content (title and content)
?>
<div class="section post">
    <div class="row">
        <div class="large-12 columns">
            <div class="post-content primary-block">

              <div class="post-body">
                <h1><span class="headline"><?php the_title(); ?></span>
                <div class="row">
                    <div class="triple-circles orange">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div></h1>
                <?php the_content(); ?>

                  <?php /*
                                  <h2 id="authors">Authors</h2>
                  <ul>
                  <?php
                  wp_list_authors(
                    array(
                      'exclude_admin' => false,
                    )
                  );
                  ?>
                  </ul>
                  */ ?>
<?php endwhile; // End post loop ?>

                  <?php hksn_sitemap_nav(); ?>
                  <?php /*<h2 id="pages">Pages</h2> ?>
                  <ul class='block-list'>
                  <?php
                  // Add pages you'd like to exclude in the exclude here
                  wp_list_pages(
                    array(
                      'exclude' => '',
                      'title_li' => '',
                    )
                  );
                  ?>
                  </ul>

                  <h3 id="posts">The Latest</h3>
                  <?php //<ul> ?>
                  <?php
                  // Add categories you'd like to exclude in the exclude here
                  $cats = get_categories('include=6');
                  foreach ($cats as $cat) {
                    //echo "<li><h4>".$cat->cat_name."</h4>";
                    echo "<ul class='block-list'>";
                    query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
                    while(have_posts()) {
                      the_post();
                      $category = get_the_category();
                      // Only display a post link once, even if it's in multiple categories
                      if ($category[0]->cat_ID == $cat->cat_ID) {
                        echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
                      }
                    }
                    echo "</ul>";
                    //echo "</li>";
                  }
                  ?>
                  <?php //</ul> */ ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/contactus-block' ) ); ?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/footer','inc/shared/html-footer' ) ); ?>