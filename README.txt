

	HKSN Base Theme

	Created by Hookson, Ltd.

	- - - - - - - - - - - - - - - - - - - - - - -

	A starter theme for Hookson projects.

	See 'CMS Configuration & Page Build Process' 
	Google doc for most recent developments and guidance.

	- - - - - - - - - - - - - - - - - - - - - - -

	Version 2.0, updated January 2017

	Stripped back all mark-up, styles and scripts.

	- - - - - - - - - - - - - - - - - - - - - - -
	- - - - - - - - - - - - - - - - - - - - - - -
	- - - - - - - - - - - - - - - - - - - - - - -
	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0

	Used ntsusa.org as base.