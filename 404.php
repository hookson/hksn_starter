<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Please see /external/starkers-utilities.php for info on HKSN_Utilities::get_template_parts()
 *
 * @package   WordPress
 * @subpackage  HKSN Base Template Kit
 */
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

        <!-- Main content -->
        <main role="main" id="maincontent">
            <div class="section">

                <div class="row">
                    <div class="small-12 columns">
                        <div class="primary widget-intro left-aligned">
                            <h1 class="page-title">Page not found</h1>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="large-8 xlarge-9 columns"> 
                      <div class="medium-12 columns">
                        <div class="general-content">
                          	<p>We're sorry but that page cannot be found.<br>
                          	You can browse the available sections using the menu above, or use our site search.</p>
                          	<p>Alternatively, please <a href="<?php echo home_url('/contact/'); ?>">contact us</a> for assistance or more information.</p>
                        </div>
                      </div>
                    </div>
        
                    <div class="large-4 xlarge-3 columns sidebar">
                        <?php HKSN_Utilities::get_template_parts( array( 'inc/shared/sidebar' ) ); ?>
                    </div>
                </div>

            </div>
        </main>
        <!-- End main content -->

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/footer','inc/shared/html-footer' ) ); ?>