<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package 	WordPress
 * @subpackage 	HKSN Base Template Kit
 */
?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
// -----------------------------------------------------------------------
// Introduction ----------------------------------------------------
// Default page content (title and content)
// If used (sometimes excluded from front page)
?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>

<?php
// -----------------------------------------------------------------------
// Content Sections (ACF) ----------------------------------------------------
// Build the page with ACF flexible content blocks
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/modules/acf_page-block' ) ); ?>

<?php endwhile; // End post loop ?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/footer','inc/shared/html-footer' ) ); ?>