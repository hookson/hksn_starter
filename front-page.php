<?php
/**
 * The template for displaying front page.
 *
 * @package 	WordPress
 * @subpackage 	HKSN Base Template Kit
 */
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header', 'inc/shared/signup-dropdown.php' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
// ---------------------------------------------------------------------
// Hero Carousel ----------------------------------------------------------
// Output hero carousel
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/modules/hero_image' ) ); ?>
<?php
// -----------------------------------------------------------------------
// Introduction ----------------------------------------------------
// Default page content (title and content)
// If used (sometimes excluded from front page)
?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php
// ---------------------------------------------------------------------
// Full width promo ----------------------------------------------------------
// Output full width promos
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/modules/promo_blocks' ) ); ?>
<?php
// -----------------------------------------------------------------------
// Content Sections (ACF) ----------------------------------------------------
// Build the page with ACF flexible content blocks
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/modules/acf_page-block' ) ); ?>

<?php endwhile; // End post loop ?>
<?php
// -----------------------------------------------------------------------
// Latest Posts ----------------------------------------------------
// Output latest 4 posts
?>
<?php HKSN_Utilities::get_template_parts( array( 'inc/modules/latest_posts' ) ); ?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/footer','inc/shared/html-footer' ) ); ?>