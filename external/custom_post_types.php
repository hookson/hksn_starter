<?php
/**
 * HKSN functions and definitions
 *
 * For custom post types  - http://codex.wordpress.org/Function_Reference/register_post_type
 * For custom taxonomies  - hattp://codex.wordpress.org/Function_Reference/register_taxonomy
 */
 
// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'hksn_flush_rewrite_rules' );
 
// Flush your rewrite rules
function hksn_flush_rewrite_rules() {
    flush_rewrite_rules();
}

// REGISTER CUSTOM POST TYPES
// Custom post type: Resource

// Custom post type: Event
function cp_event() { 
  
  // creating (registering) the custom type 
  register_post_type( 'event',
    
    // add all the options for this post type
    array( 'labels' => array(
      'name' => __( 'Events', 'hksntheme' ), 
      'singular_name' => __( 'Event', 'hksntheme' ),
      'all_items' => __( 'All Events', 'hksntheme' ),
      'add_new' => __( 'Add New', 'hksntheme' ),
      'add_new_item' => __( 'Add New Event', 'hksntheme' ),
      'edit' => __( 'Edit', 'hksntheme' ),
      'edit_item' => __( 'Edit Event', 'hksntheme' ),
      'new_item' => __( 'New Event', 'hksntheme' ), 
      'view_item' => __( 'View Event', 'hksntheme' ), 
      'search_items' => __( 'Search Events', 'hksntheme' ), 
      'not_found' =>  __( 'Nothing found in the Database.', 'hksntheme' ),
      'not_found_in_trash' => __( 'Nothing found in Trash', 'hksntheme' ),
      'parent_item_colon' => ''
      ),
      'description' => __( '', 'hksntheme' ), 
      'public' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'show_ui' => true,
      'query_var' => true,
      'taxonomies' => array( 'category' ),
      'menu_position' => 5, 
      //'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', 
      'rewrite'   => array( 'slug' => 'event', 'with_front' => false ),
      'has_archive' => 'events', 
    'show_in_rest' => true,
    'rest_base' => 'events',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
      'capability_type' => 'post',
      'hierarchical' => false,
      
      'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes')
    ) /* end of options */
  ); /* end of register post type */
   
  /* this adds your post categories to your custom post type */
  //register_taxonomy_for_object_type( 'category', 'people' );
  /* this adds your post tags to your custom post type */
  //register_taxonomy_for_object_type( 'post_tag', 'people' );

}

// adding the functions to the Wordpress init
add_action( 'init', 'cp_event');
 
// REGISTER CUSTOM TAXONOMIES (these act like categories)
// Custom taxonomy (category): Region
register_taxonomy( 'region', 
    array('post','event'), /* if you change the name of Custom post type - register_post_type( 'custom_type', */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Region', 'hksntheme' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Region', 'hksntheme' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Regions', 'hksntheme' ), /* search title for taxomony */
            'all_items' => __( 'All Regions', 'hksntheme' ), /* all title for taxonomies */
            'parent_item' => __( 'Parent Region', 'hksntheme' ), /* parent title for taxonomy */
            'parent_item_colon' => __( 'Parent Region:', 'hksntheme' ), /* parent taxonomy title */
            'edit_item' => __( 'Edit Region', 'hksntheme' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Region', 'hksntheme' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Region', 'hksntheme' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Region Name', 'hksntheme' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true, 
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'regions'),
        // If required for REST functionality (see ICF)
        //'show_in_rest'       => true,
  		//'rest_base'          => 'regions',
  		//'rest_controller_class' => 'WP_REST_Terms_Controller', 
    )
);
// Custom taxonomy (tag): Event Tags
register_taxonomy( 'event_tags', 
    array('event'), /* if you change the name of Custom post type - register_post_type( 'custom_type', */
    array('hierarchical' => false,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Event Tag', 'hksntheme' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Event Tag', 'hksntheme' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Event Tags', 'hksntheme' ), /* search title for taxomony */
            'all_items' => __( 'All Event Tags', 'hksntheme' ), /* all title for taxonomies */
            'parent_item' => __( 'Parent Event Tag', 'hksntheme' ), /* parent title for taxonomy */
            'parent_item_colon' => __( 'Parent Event Tag:', 'hksntheme' ), /* parent taxonomy title */
            'edit_item' => __( 'Edit Event Tag', 'hksntheme' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Event Tag', 'hksntheme' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Event Tag', 'hksntheme' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Event Tag Name', 'hksntheme' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true, 
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'event_tags'),
        
    )
);
     
/**
* Custom post type icons - http://calebserna.com/dashicons-cheatsheet/
*/
function add_menu_icons_styles(){
?>  
  <style>
  #adminmenu #menu-posts div.wp-menu-image:before {
    color: #ecd476;
  }
  #adminmenu #menu-posts div.wp-menu-name {
    color: #ecd476;
  }
  #adminmenu #menu-pages div.wp-menu-image:before {
    color: #fd8a8a;
  }
  #adminmenu #menu-pages div.wp-menu-name {
    color: #fd8a8a;
  }
  #adminmenu #menu-posts-event div.wp-menu-image:before {
    content: '\f145';
    color: #f5b183;
  }
  #adminmenu #menu-posts-event div.wp-menu-name {
    color: #f5b183;
  }
  </style>
<?php
}

add_action( 'admin_head', 'add_menu_icons_styles' );

?>