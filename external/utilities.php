<?php

	/**
	 * HKSN_Utilities
	 *
	 * Based on Starkers Utilities Class v.1.1
	 *
	 */
	 
	 class HKSN_Utilities {

    	/**
    	 * Print a pre formatted array to the browser - very useful for debugging
    	 *
    	 * @param 	array
    	 * @return 	void
    	 * @author 	Keir Whitaker
    	 **/
    	public static function print_a( $a ) {
    		print( '<pre>' );
    		print_r( $a );
    		print( '</pre>' );
    	}

    	/**
    	 * Simple wrapper for native get_template_part()
    	 * Allows you to pass in an array of parts and output them in your theme
    	 * e.g. <?php get_template_parts(array('part-1', 'part-2')); ?>
    	 *
    	 * @param 	array 
    	 * @return 	void
    	 * @author 	Keir Whitaker
    	 **/
    	public static function get_template_parts( $parts = array() ) {
    		foreach( $parts as $part ) {
    			get_template_part( $part );
    		};
    	}
        /**
         * Simple wrapper for native locate_template()
         * Allows you to pass in an array of parts and output them in your theme 
         * while also passing variables when required
         * e.g. <?php get_template_parts(array('part-1', 'part-2')); ?>
         *
         * @param   array 
         * @return  void
         * @author  Keir Whitaker
         **/
        public static function locate_templates( $parts = array() ) {
            foreach( $parts as $part ) {
                locate_template( $part );
            };
        }



    	/**
    	 * Pass in a path and get back the page ID
    	 * e.g. HKSN_Utilities::get_page_id_from_path('about/terms-and-conditions');
    	 *
    	 * @param 	string 
    	 * @return 	integer
    	 * @author 	Keir Whitaker
    	 **/
    	public static function get_page_id_from_path( $path ) {
    		$page = get_page_by_path( $path );
    		if( $page ) {
    			return $page->ID;
    		} else {
    			return null;
    		};
    	}

    	/**
    	 * Append page slugs to the body class
    	 * NB: Requires init via add_filter('body_class', 'add_slug_to_body_class');
    	 *
    	 * @param 	array 
    	 * @return 	array
    	 * @author 	Keir Whitaker
    	 */
    	public static function add_slug_to_body_class( $classes ) {
    		global $post;
	   
    		if( is_page() ) {
    			$classes[] = sanitize_html_class( $post->post_name );
    		} elseif(is_singular()) {
    			$classes[] = sanitize_html_class( $post->post_name );
    		};

    		return $classes;
    	}
	
    	/**
    	 * Get the category id from a category name
    	 *
    	 * @param 	string 
    	 * @return 	string
    	 * @author 	Keir Whitaker
    	 */
    	public static function get_category_id( $cat_name ){
    		$term = get_term_by( 'name', $cat_name, 'category' );
    		return $term->term_id;
    	}

        /**
         * Format State
         * HT: https://gist.github.com/maxrice/2776900
         *
         * Note: Does not format addresses, only states. $input should be as exact as possible, problems
         * will probably arise in long strings, example 'I live in Kentukcy' will produce Indiana.
         *
         * @example echo myClass::format_state( 'Florida', 'abbr'); // FL
         * @example echo myClass::format_state( 'we\'re from georgia' ) // Georgia
         * 
         * @param  string $input  Input to be formatted
         * @param  string $format Accepts 'abbr' to output abbreviated state, default full state name.
         * @return string          Formatted state on success, 
         */
        public static function format_state( $input, $format = '' ) {
            if( ! $input || empty( $input ) )
                return;

            $states = array (
                'AL'=>'Alabama',
                'AK'=>'Alaska',
                'AZ'=>'Arizona',
                'AR'=>'Arkansas',
                'CA'=>'California',
                'CO'=>'Colorado',
                'CT'=>'Connecticut',
                'DE'=>'Delaware',
                'DC'=>'District Of Columbia',
                'FL'=>'Florida',
                'GA'=>'Georgia',
                'HI'=>'Hawaii',
                'ID'=>'Idaho',
                'IL'=>'Illinois',
                'IN'=>'Indiana',
                'IA'=>'Iowa',
                'KS'=>'Kansas',
                'KY'=>'Kentucky',
                'LA'=>'Louisiana',
                'ME'=>'Maine',
                'MD'=>'Maryland',
                'MA'=>'Massachusetts',
                'MI'=>'Michigan',
                'MN'=>'Minnesota',
                'MS'=>'Mississippi',
                'MO'=>'Missouri',
                'MT'=>'Montana',
                'NE'=>'Nebraska',
                'NV'=>'Nevada',
                'NH'=>'New Hampshire',
                'NJ'=>'New Jersey',
                'NM'=>'New Mexico',
                'NY'=>'New York',
                'NC'=>'North Carolina',
                'ND'=>'North Dakota',
                'OH'=>'Ohio',
                'OK'=>'Oklahoma',
                'OR'=>'Oregon',
                'PA'=>'Pennsylvania',
                'RI'=>'Rhode Island',
                'SC'=>'South Carolina',
                'SD'=>'South Dakota',
                'TN'=>'Tennessee',
                'TX'=>'Texas',
                'UT'=>'Utah',
                'VT'=>'Vermont',
                'VA'=>'Virginia',
                'WA'=>'Washington',
                'WV'=>'West Virginia',
                'WI'=>'Wisconsin',
                'WY'=>'Wyoming',
            );

            foreach( $states as $abbr => $name ) {
                if ( preg_match( "/\b($name)\b/", ucwords( strtolower( $input ) ), $match ) )  {
                    if( 'abbr' == $format ){ 
                        return $abbr;
                    } 
                    else return $name;
                }
                elseif( preg_match("/\b($abbr)\b/", strtoupper( $input ), $match) ) {                    
                    if( 'abbr' == $format ){ 
                        return $abbr;
                    } 
                    else return $name;
                } 
            }
            return;
        }
	
    }
    
// Borrowed from FoundationPress
class Top_Bar_Walker extends Walker_Nav_Menu {
    function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
        $element->has_children = ! empty( $children_elements[ $element->ID ] );
        $element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
        $element->classes[] = ( $element->has_children && 1 !== $max_depth ) ? 'has-dropdown' : '';
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
    function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
        $item_html = '';
        parent::start_el( $item_html, $object, $depth, $args );
        $output .= ( 0 == $depth ) ? '<li class="divider"></li>' : '';
        $classes = empty( $object->classes ) ? array() : (array) $object->classes;
        if ( in_array( 'label', $classes ) ) {
            $output .= '<li class="divider"></li>';
            $item_html = preg_replace( '/<a[^>]*>(.*)<\/a>/iU', '<label>$1</label>', $item_html );
        }
    if ( in_array( 'divider', $classes ) ) {
        $item_html = preg_replace( '/<a[^>]*>( .* )<\/a>/iU', '', $item_html );
    }
        $output .= $item_html;
    }
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= "\n<ul class=\"dropdown dropdown-wrapper\">\n";
    }
}

class Offcanvas_Walker extends Walker_Nav_Menu {
    function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
        $element->has_children = ! empty( $children_elements[ $element->ID ] );
        $element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
        $element->classes[] = ( $element->has_children && 1 !== $max_depth ) ? 'has-submenu' : '';
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
    function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
        $item_html = '';
        parent::start_el( $item_html, $object, $depth, $args );
        $classes = empty( $object->classes ) ? array() : (array) $object->classes;
        if ( in_array( 'label', $classes ) ) {
            $item_html = preg_replace( '/<a[^>]*>(.*)<\/a>/iU', '<label>$1</label>', $item_html );
        }
        $output .= $item_html;
    }
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= "\n<ul class=\"left-submenu\">\n<li class=\"back\"><a href=\"#\">". __( 'Back', 'hksntheme' ) ."</a></li>\n";
    }
}