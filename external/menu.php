<?php
// Register menus
register_nav_menus(
    array(
        'primary' => __( 'Primary Navigation', 'hksntheme' ),   // Main nav in header
        'secondary' => __( 'Secondary Navigation', 'hksntheme' ),   // Secondary nav in header
        'in_page' => __( 'Site Map', 'hksntheme' ),   // Site Map nav in page
        'footer-links1' => __( 'Footer Links Column 1', 'hksntheme' ), // Other nav in footer
        'footer-links2' => __( 'Footer Links Column 2', 'hksntheme' ), // Other nav in footer
        'footer-links3' => __( 'Footer Links Column 3', 'hksntheme' ), // Other nav in footer
        'footer-links4' => __( 'Footer Links Column 4', 'hksntheme' ) // Other nav in footer
    )
);

// The Primary Navigation Menu
function hksn_primary_nav() {
     wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'container_class' => '',                        // Class of container
        'menu' => 'Primary Navigation', 'hksntheme',    // Menu name
        'items_wrap'    => '%3$s',                      // Remove ul container
        'menu_class' => 'right megamenu',               // Adding custom nav class
        'theme_location' => 'primary',                  // Where it's located in the theme
        'before' => '',                                 // Before each link <a>
        'after' => '',                                  // After each link </a>
        'link_before' => '',                            // Before each link text
        'link_after' => '',                             // After each link text
        'depth' => 5,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Top_Bar_Walker(),
    ));
} /* End Primary Navigation Menu */

// The Secondary Navigation Menu
function hksn_secondary_nav() {
     wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'container_class' => '',                        // Class of container
        'menu' => 'Secondary Navigation', 'hksntheme',                                   // Menu name
        'menu_class' => 'right',            // Adding custom nav class
        'theme_location' => 'secondary',                // Where it's located in the theme
        'before' => '',                                 // Before each link <a>
        'after' => '',                                  // After each link </a>
        'link_before' => '',                            // Before each link text
        'link_after' => '',                             // After each link text
        'depth' => 5,                                   // Limit the depth of the nav
        'items_wrap' => '%3$s',
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Top_Bar_Walker(),
    ));
} /* End Secondary Navigation Menu */

// The Site Map Menu
function hksn_sitemap_nav() {
     wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'container_class' => '',                        // Class of container
        'menu' => 'Site Map', 'hksntheme',                                   // Menu name
        'menu_class' => 'block-list',            // Adding custom nav class
        'theme_location' => 'in_page',                // Where it's located in the theme
        'before' => '',                                 // Before each link <a>
        'after' => '',                                  // After each link </a>
        'link_before' => '',                            // Before each link text
        'link_after' => '',                             // After each link text
        'depth' => 5,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
    ));
} /* End Secondary Navigation Menu */

// The Footer Menus (4 sections) 
// Check for better way of doing this (avoiding 4 x functions!)
// The Footer Menu - Column 1 
function hksn_footer_links1() {
    wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'container_class' => '',                        // Class of container
        'menu' => 'Footer Navigation - Column 1', 'hksntheme',    // Menu name
        'menu_class' => 'sitemap-list',                 // Adding custom nav class
        'theme_location' => 'footer-links1',            // Where it's located in the theme
        'before' => '',                                 // Before each link <a>
        'after' => '',                                  // After each link </a>
        'link_before' => '',                            // Before each link text
        'link_after' => '',                             // After each link text
        'depth' => 2,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Top_Bar_Walker(),
    ));
} /* End Footer Menu - Column 1  */
// The Footer Menu - Column 2 
function hksn_footer_links2() {
    wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'container_class' => '',                        // Class of container
        'menu' => 'Footer Navigation - Column 2', 'hksntheme',    // Menu name
        'menu_class' => 'sitemap-list',                 // Adding custom nav class
        'theme_location' => 'footer-links2',            // Where it's located in the theme
        'before' => '',                                 // Before each link <a>
        'after' => '',                                  // After each link </a>
        'link_before' => '',                            // Before each link text
        'link_after' => '',                             // After each link text
        'depth' => 2,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Top_Bar_Walker(),
    ));
} /* End Footer Menu - Column 2  */
// The Footer Menu - Column 3 
function hksn_footer_links3() {
    wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'container_class' => '',                        // Class of container
        'menu' => 'Footer Navigation - Column 3', 'hksntheme',    // Menu name
        'menu_class' => 'sitemap-list',                 // Adding custom nav class
        'theme_location' => 'footer-links3',            // Where it's located in the theme
        'before' => '',                                 // Before each link <a>
        'after' => '',                                  // After each link </a>
        'link_before' => '',                            // Before each link text
        'link_after' => '',                             // After each link text
        'depth' => 2,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Top_Bar_Walker(),
    ));
} /* End Footer Menu - Column 3  */
// The Footer Menu - Column 4 
function hksn_footer_links4() {
    wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'container_class' => '',                        // Class of container
        'menu' => 'Footer Navigation - Column 4', 'hksntheme',    // Menu name
        'menu_class' => 'sitemap-list',                 // Adding custom nav class
        'theme_location' => 'footer-links4',            // Where it's located in the theme
        'before' => '',                                 // Before each link <a>
        'after' => '',                                  // After each link </a>
        'link_before' => '',                            // Before each link text
        'link_after' => '',                             // After each link text
        'depth' => 2,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Top_Bar_Walker(),
    ));
} /* End Footer Menu - Column 4  */

// Header Fallback Menu
function hksn_main_nav_fallback() {
    wp_page_menu( array(
        'show_home' => true,
        'menu_class' => '',      // Adding custom nav class
        'include'     => '',
        'exclude'     => '',
        'echo'        => true,
        'link_before' => '',                            // Before each link
        'link_after' => ''                             // After each link
    ) );
}

// Footer Fallback Menu
function hksn_footer_links_fallback() {
    /* You can put a default here if you like */
}

function hksn_list_child_pages() { 

    global $post; 

    if ( is_page() && $post->post_parent )

        $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
    else
        $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );

    if ( $childpages ) {

        $string = '<ul>' . $childpages . '</ul>';
    }

    return $string;

}
add_shortcode('hksn_childpages', 'hksn_list_child_pages');
