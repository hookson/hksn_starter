<?php
/**
 * Template name: Events
 * Used by: 
 *
 * @package   WordPress
 * @subpackage  HKSN Base Template Kit
 */
?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
// -----------------------------------------------------------------------
// Introduction ----------------------------------------------------
// Default page content (title and content)
// If used (sometimes excluded from front page)
?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>

<?php endwhile; // End post loop ?>
<div class="row">
<?php
// -----------------------------------------------------------------------
// Output events ----------------------------------------------------
// Show events for today or future

$event_query = new WP_Query( 
  array(
    'posts_per_page' => '5',
    'post_type' => 'event',
    'meta_key'  => 'event_start_date',
    'orderby'   => 'meta_value_num',
    'order' => 'ASC',
		'meta_query'  => array(
      array(
        'key' => 'event_start_date',
        'type' => 'NUMERIC', // MySQL needs to treat date meta values as numbers
        'value' => current_time( 'Ymd' ), // Today in ACF datetime format
        'compare' => '>=', // Greater than or equal to value
      ),
    ),
  )
);
?>
<?php
    
  $count = 1;
  // Output posts 
  while ( $event_query->have_posts() ) {

    $event_query->the_post();
?>
<?php include( locate_template( 'inc/modules/loop_event.php' ) ); ?>
<?php
  $count++;
  // End: Output posts (foreach)
}
?>
</div>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/signup-block','inc/shared/footer','inc/shared/html-footer' ) ); ?>