<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/utilities.php for info on HKSN_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	HKSN Base Template Kit
 */
?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/html-header', 'inc/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
// -----------------------------------------------------------------------
// Introduction ----------------------------------------------------
// Default page content (title and content)
// If used (sometimes excluded from front page)
?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>

<?php 
// -----------------------------------------------------------------------
// Authorship Information (ACF) ------------------------------------------
// Author information for the post with ACF content blocks

$author_image        = get_field('author_image');
$author_image_size   = 'thumbnail';

if( get_field('author_name') ) {
    $author_name = "<h5>" . get_field('author_name') . "</h5>";
}
if( get_field('author_position') ) {
    $author_position = "<strong>" . get_field('author_position') . "</strong><br>";
}
if( get_field('author_company') ) {
    $author_company = "<strong>" . get_field('author_company') . "</strong><br>";
}
if( get_field('author_twitter') ) {
    $author_twitter = "<strong><a href='https://www.twitter.com/" . get_field('author_twitter') . "'>" . get_field('author_twitter') . "</a></strong><br>";
}
if( get_field('author_website') ) {
    $author_website = "<strong><a href='" . get_field('author_website') . "'>" . get_field('author_website') . "</a></strong>";
}
?>
<?php if ( $author_name ) { ?>
<div>
<h3>About the Author</h3>

    <?php if ( get_field('author_image') ) { ?>
        <?php echo wp_get_attachment_image( $author_image, $author_image_size ); ?>
    <?php } ?>

    <?php echo $author_name; ?>
    <?php echo $author_position; ?>
    <?php echo $author_company; ?>
    <?php echo $author_twitter; ?>
    <?php echo $author_website; ?>

    <?php if ( get_field('author_biography') ) { ?>
        <?php the_field('author_biography'); ?>
    <?php } ?>

<?php } ?>
</div>

<?php
// -----------------------------------------------------------------------
// Post meta ----------------------------------------------------
// Meta content for the post content (published, tags etc)
?>
<p>Published: <?php the_date('j F Y'); ?></p>
<p>Categories: 
<?php
    $args = array(
        //default to current post
        'post' => 0,
        'before' => '',
        //this is the default
        'sep' => ' ',
        'after' => '',
        //this is the default
        //'template' => '<li class="tag-label">%s <ul>%l</ul></li>'
    );
    echo get_the_term_list( $post->ID, 'category', '<div class="tags">', ' ', '</div>' ); ?>
</p>

<?php
// -----------------------------------------------------------------------
// Share this ----------------------------------------------------
// Links to share page content
?>
<div>
<span>Share this post:</span>
<a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=<?php the_permalink(); ?>&amp;pubid=ra-55c9b19dd297c33b&amp;ct=1&amp;title=<?php the_title(); ?>&amp;pco=tbxnj-1.0" target="_blank" class="facebook">
    <i class="fa fa-facebook"></i>
</a>
<a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=<?php the_permalink(); ?>&amp;pubid=ra-55c9b19dd297c33b&amp;ct=1&amp;title=<?php the_title(); ?>&amp;pco=tbxnj-1.0" target="_blank" class="twitter">
    <i class="fa fa-twitter"></i>
</a>
<a href="https://api.addthis.com/oexchange/0.8/forward/google_plusone_share/offer?url=<?php the_permalink(); ?>&amp;pubid=ra-55c9b19dd297c33b&amp;ct=1&amp;title=<?php the_title(); ?>&amp;pco=tbxnj-1.0" target="_blank" class="google-plus">
    <i class="fa fa-google-plus"></i>
</a>
<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>&amp;source=National Trust for Scotland USA" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="linkedin" title="Share this on Linkedin">
    <i class="fa fa-linkedin"></i>
</a>
<a href="mailto:?subject=<?php the_title(); ?>&amp;Body=<?php the_permalink(); ?>" class="email" title="Share this by email" target="_blank">
    <i class="fa fa-envelope"></i>
</a>
</div>
<?php endwhile; // End post loop ?>

<?php
// -----------------------------------------------------------------------
// Comments ----------------------------------------------------
// 
?>
<?php comments_template(); ?>

<?php
// -----------------------------------------------------------------------
// Post Navigation ----------------------------------------------------
// Links to previous post
?>
<?php 
    $prevPost = get_previous_post(true);

    if($prevPost): 
?>
<?php

function the_excerpt_max_charlength($charlength,$excerpt) {

    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
            echo mb_substr( $subex, 0, $excut );
        } else {
            echo $subex;
        }
        echo '[...]';
    } else {
        echo $excerpt;
    }
}

    $prevthumbnail = get_the_post_thumbnail( $prevPost->ID, array(270,270) );
    
    $temp = $post;
    $post = get_post( $prevPost->ID );
    setup_postdata( $post );
    
    $prevexcerpt = get_the_excerpt();  

    wp_reset_postdata(); 
    $post = $temp;
      
?>
<?php echo $prevthumbnail; 
/*?><img width="270" height="270" src="" class="attachment-270x270 size-270x270 wp-post-image" alt="VY8W7693" /> */ ?>
<?php previous_post_link('%link','%title', TRUE); ?>

<?php endif; // /if($prevPost):  ?>

<?php HKSN_Utilities::get_template_parts( array( 'inc/shared/signup-block','inc/shared/footer','inc/shared/html-footer' ) ); ?>