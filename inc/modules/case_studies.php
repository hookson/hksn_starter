<?php
// Theme part for Case studies blocks
?>
<div class="section light-section profiles alumni-profiles">
    <div class="row">
<?php
    // Output blocks    
    while( has_sub_field('interview_block') ):

        // Prepare content
        $int_image_id = get_sub_field('int_image');
        $int_image = wp_get_attachment_image_src($int_image_id, 'Profile');
?>
        <div class="large-12 columns">
            <div class="widget widget-landscape widget-profile">
                <div class="w-content">
                    <?php if( $int_image ) { ?>
                    <div class="w-image left">
                        <img src="<?php echo $int_image[0]; ?>" alt="<?php the_title(); ?>" />
                    </div>
                    <?php } ?>
                    <div class="w-body right">
                        <header class="w-header">
                            <h2 class="section-title"><?php the_sub_field('int_name'); ?>, <?php the_sub_field('int_role'); ?></h2>
                        </header>
                        <p class="company"><?php the_sub_field('int_company'); ?></p>
                        <?php the_sub_field('int_questions'); ?>
                    </div>
                </div>
            </div>
        </div>
<?php
    // End: Output blocks (while)
    endwhile;
?>
    </div>
</div>