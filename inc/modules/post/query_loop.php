<?php
// Template:        All Posts Query Loop
// Type:            Parial/Query Loop
// Notes:
// Output posts wp_query loop (e.g. latest_posts.php)
?>
<?php 
  // Prepare content
        $post_id = $recent['ID'];
        $image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'Teaser' );
        $image = $image_array['0'];
?>
<div class="xlarge-3 large-4 columns">
  <a tabindex="0" href="<?php echo get_permalink($post_id); ?>"><img src="<?php echo $image; ?>"></a>
  <h3><a tabindex="0" href="<?php echo get_permalink($post_id); ?>"><?php echo $recent["post_title"]; ?></a></h3>
  <p><?php echo $recent["post_excerpt"]; ?></p>
</div>