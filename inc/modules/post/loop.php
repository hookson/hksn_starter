<?php
// Template:        All Posts Loop
// Type:            Parial/Loop
// Notes:
// Output posts loop (tags.php, archive.php)
?>
<?php 
    // Prepare content
    //$large_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'Teaser' );
    //$large_image = $large_image_array['0'];
    $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'TeaserSmall' );
    $image = $image_array['0'];

    $content = get_the_excerpt();
    $content = apply_filters('the_excerpt', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $short_content = wp_trim_words( $content, 26, $more = '� ' );
?>
                    <div class="large-6 xlarge-4 columns hide-for-large-only end widget-col mini">
                        <div class="widget widget-portrait widget-portrait--regular w-clickable matchHeight">
                            <div class="w-content">
                                <div class="w-image">
                                    <figure>
                                        <a href="<?php esc_url( the_permalink() ); ?>"><img src="<?php echo $image; ?>"></a>
                                    </figure>
                                </div>
                                <div class="w-meta">
                                    <p class="date"><?php the_date('d M Y'); // 02 Nov 2016 ?></p>
                                </div>
                                <div class="w-body">
                                    <h4 class="widget-title"><?php the_title(); ?></h4>
                                    <p><?php echo $short_content; ?></p>
                                </div>
                                <a href="<?php esc_url( the_permalink() ); ?>" class="link-overlay" title="Widget link"></a>
                            </div>
                        </div>
                    </div>