<?php

// Theme part for CTA blocks (e.g. EIE Scotland and London on HP)

?>

    <div class="services-wrapper">

        <div class="row">

            <div class="large-12 columns">

                <div class="row">

<?php

	// Output blocks

    while( has_sub_field('cta_blocks') ):



		// Prepare content

        $ctab_image_id = get_sub_field('ctab_image');

        $ctab_image = wp_get_attachment_image_src($ctab_image_id, 'CTA_block');



        $ctab_logo_id = get_sub_field('ctab_logo');

        $ctab_logo = wp_get_attachment_image_src($ctab_logo_id, 'CTA_block');

?>



                    <div class="large-6 columns widget-image-link">

                        <div class="bg" style="background-image: url(<?php echo $ctab_image[0]; ?>); background-repeat: no-repeat; background-size: cover; height: 500px;">

                            <div class="hero-overlay"></div>

                            <div class="figcaption">

                                <div class="figcaption-centered">

                                    <h2><img src="<?php echo $ctab_logo[0]; ?>" alt="<?php the_sub_field('ctab_headline'); ?>"></h2>

                                    <p><?php the_sub_field('ctab_text'); ?></p>



                                    <a href="<?php the_sub_field('ctab_url'); ?>" class="button button-3"><?php the_sub_field('ctab_button_label'); ?></a>

                                </div>

                            </div>

                        </div>

                    </div>

<?php

	// End: Output blocks (while)

	endwhile;

?>



                </div>

            </div>

        </div>

    </div>