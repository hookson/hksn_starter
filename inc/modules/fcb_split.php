<?php
// Theme part for Tabbed page template

?>

<div<?php echo ' class="tab_option'.$tab_option.'"'; ?>>
<div class="section tabbed" id="tabs">
    <div class="row">
        <div class="large-12 columns">
            <ul class="tabbed-nav<?php echo " taboptn_".$tab_option; ?>">
                <li class="tab1_link"><a href="?tab_option=1#tabs">Option 1</a></li>
                <li class="tab2_link"><a href="?tab_option=2#tabs">Option 2</a></li>
                <li class="tab3_link"><a href="?tab_option=3#tabs">Option 3</a></li>
                <li class="tab4_link"><a href="?tab_option=4#tabs">Option 4</a></li>
            </ul>
        </div>
    </div>
</div>

<?php

// check if the flexible content field has rows of data
if( have_rows('page_block') ):
    
    $count = 1;

     // loop through the rows of data
    while ( have_rows('page_block') ) : the_row();

// ---------------------------------------------------------------------
// New Block -----------------------------------------------------------
// Image and Summary 'teaser block' - Portrait
?>
<div id="<?php echo "tab_".$count; ?>">
<?php
    if( get_sub_field( 'pbis_block_title') ): 

    $pbis_shaded_block = " padded"; // Add padding as this is a new block

?>
<div class="section heading">
    <div class="row">
        <div class="large-12 columns">
            <div class="widget-text">
                <h2 class="section-title"><?php the_sub_field('pbis_block_title'); ?></h2>
                <?php if( get_sub_field( 'pbis_block_summary') ){ the_sub_field('pbis_block_summary'); } ?>
            </div>
        </div>
    </div>
</div>
<?php        
            endif; // if( get_field( 'pbab_block_title') ):

            if( have_rows('pbis_block_content') ):
?>

<div class="section shaded<?php echo $pbis_shaded_block; ?>">
    <div class="row">
    <?php
    // loop through the rows of data
        while ( have_rows('pbis_block_content') ) : the_row();

            $pbis_image        = get_sub_field('pbis_item_image');
            $pbis_image_size   = 'Teaser';
            $pbis_copy         = get_sub_field('pbis_item_copy');
            //echo $pbis_item_image." AND ".$pbis_item_copy;

            // Get caption for this image (if exists)
            $pbis_attachments = get_post($pbis_image);
            $pbis_image_caption = $pbis_attachments->post_excerpt;
    ?>
<?php if( get_sub_field( 'pbis_jump_link_id') ){ ?>
    <div id='<?php the_sub_field('pbis_jump_link_id'); ?>'>
<?php } ?>
        <!-- Widget landscape -->
        <div class="widget widget-landscape--split">
            <div class="w-content row">
                <div class="large-6 columns w-image left matchheight">
                    <?php echo wp_get_attachment_image( $pbis_image, $pbis_image_size ); ?>
                    <?php if($pbis_image_caption){ echo "<span class='w-caption'>".$pbis_image_caption."</span>"; } ?>
                </div>
                <div class="large-6 columns right matchheight">
                    <div class="w-body left-text">
                        <?php the_sub_field('pbis_item_copy'); ?>
                    </div>
                </div>
            </div>
        </div>
<?php if( get_sub_field( 'pbis_jump_link_id') ){ ?>
    </div>
<?php } ?>
    <?php
        endwhile;
    ?>
    </div>
</div>
</div><?php /*<div id="<?php echo "tabbed_".$count; ?>"> */ ?>
<?php 

endif;   
    
    $count++;          

    endwhile; //while ( have_rows('page_block') ) : the_row();

else :

    // no layouts found

endif;     
?>
</div>