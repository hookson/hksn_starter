<?php
// Template:        Widget Default
// Type:            Parial/Widget
// Notes:
// Used for general promos etc

// Prepare vars
// NOTE
//

?>
                        <div class="widget widget-portrait widget-portrait--extended w-large matchHeight">
                            <div class="w-content">
                                <div class="w-image">
                                    <figure>
                                        <?php echo wp_get_attachment_image( $pb_up_block_widgets_image, $pb_up_block_widgets_image_size ); ?>
                                        <?php //if($pb_2up_block_widgets_image_caption){ echo "<span class='w-caption'>".$pb_2up_block_widgets_image_caption."</span>"; } ?> 
                                    </figure>
                                </div>
                                <div class="w-body">
                                  <h3 class="widget-title">
                                    <?php if ($pb_up_block_widgets_link) { ?>
                                    <a href="<?php echo $pb_up_block_widgets_link; ?>" class="link-overlay" title="Widget link"><?php echo $pb_up_block_widgets_title; ?></a>
                                    <?php } else { ?>
                                      <?php echo $pb_up_block_widgets_title; ?>
                                    <?php } ?>
                                  </h3>
                                    <?php if( $pb_up_block_widgets_content ){ 
                                      echo $pb_up_block_widgets_content; 
                                    } ?>
                                </div>
                            </div>
                        </div>
            