<?php
// Template:        Widget Boxed
// Type:            Parial/Widget
// Notes:
// Used for case studies/testimonials

// Prepare vars
// NOTE
//

?>
                        <div class="widget widget-portrait widget-portrait--card w-clickable matchHeight">
                            <div class="w-content">
                                <?php if( $pb_up_block_widgets_label ){ ?><div class="w-tag"><?php echo $pb_up_block_widgets_label; ?></div><?php } ?>
                                <div class="w-image">
                                    <figure>
                                        <?php echo wp_get_attachment_image( $pb_up_block_widgets_image, $pb_up_block_widgets_image_size ); ?>
                                        <?php //if($pb_2up_block_widgets_image_caption){ echo "<span class='w-caption'>".$pb_2up_block_widgets_image_caption."</span>"; } ?> 
                                    </figure>
                                </div>
                                <div class="w-body">
                                    <h4 class="widget-title"><?php $pb_up_block_widgets_title; ?></h4>
                                    <?php if( $pb_up_block_widgets_content ){ 
                                      echo $pb_up_block_widgets_content; 
                                    } ?>
                                  <? /* 
                                    <blockquote>
                                        <?php if( get_sub_field( 'pb_2up_block_widgets_content') ){ 
                                            the_sub_field('pb_2up_block_widgets_content'); 
                                        } ?>
                                    </blockquote>
                                    <p class="author">John Smith, Dalkeith, Midlothian</p>*/ ?>
                                </div>
        
                                <?php if ($pb_up_block_widgets_link) { ?><a href="<?php echo $pb_up_block_widgets_link; ?>" class="link-overlay" title="Widget link"></a><?php } ?>
                            </div>
                        </div>
            