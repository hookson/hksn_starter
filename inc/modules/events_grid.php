<?php
// Theme part for Latest news carousel
?>
        <ul class="location-grid row">
<?php
	// Output blocks	
    while( has_sub_field('event_blocks') ):

		// Prepare content
        $eb_image_id = get_sub_field('image');
        $eb_image = wp_get_attachment_image_src($eb_image_id, 'medium');
?>

        <li class="rollover-card" style="background-image: url(<?php echo $eb_image[0]; ?>);">
            <div >
                <span class="hidden-block">
                    <span class="content">
                        <span class="content-cell">
                            <h5><?php the_sub_field('headline_'); ?></h5>
                            <hr class="divider">
                            <span><?php the_sub_field('text_'); ?></span>
                            <a href="<?php the_sub_field('url_'); ?>" class="button button-2"><?php the_sub_field('button_label_'); ?></a>
                        </span>
                    </span>
                </span>
                <span class="active-block">
                    <span class="container">
                        <span class="text-content">
                            <h5><?php the_sub_field('headline_'); ?></h5>
                            <hr class="divider">
                        </span>
                    </span>
                </span>
            </div>
        </li>
<?php
	// End: Output blocks (while)
	endwhile;
?>
	   </ul>