<?php
// Theme part for Events calendar custom post output
?>
<?php

    // Define taxonomy based on page_id
    if( is_page(32)) { // IVTuesday
      $page_term = "ivtuesday";

    }elseif( is_page(34)) { // Exec Forum
      $page_term = "executive-forum";
    }else { // All events
      $page_term = "";
    }

    if($page_term){
        $event_query = new WP_Query( array(
            'post_type' => 'event',
            'tax_query' => array(
                array(
              'taxonomy' => 'event type',
              'terms'    => array($page_term),
              'field'    => 'slug',
                )
            ),
            'meta_key'  => 'ev_start_date',
            'orderby'   => 'meta_value_num',
            'order' => 'ASC'
            )
        );
    } else {
        $event_query = new WP_Query( array(
            'post_type' => 'event',
            'meta_key'  => 'ev_start_date',
            'orderby'   => 'meta_value_num',
            'order' => 'ASC'
            )
        );
    }
?>
<div id="latest-news" class="section dark-section full-width w-bg-container">
    <div class="row">
<?php
    
    $count = 1;
	// Output posts	
	while ( $event_query->have_posts() ) 
        {
            $event_query->the_post();

    		// Prepare content
            $postid = get_the_ID();
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'MenuImage' );
            $image = $image_array['0'];

            // Get custom tax (terms)
            //$term_list = wp_get_post_terms( get_the_ID(), 'Group', array("fields" => "names"));
            $terms = get_the_terms( $postid, 'event type' );
            if ( $terms && ! is_wp_error( $terms ) ) {

                $event_types = array();

                foreach ( $terms as $term ) {
                    $event_types[] = $term->name;
                }
                                    
                $event_type = join( ", ", $event_types );

            }
/*
            $event_types = get_terms( 
                'event type', 
                array(
                  'orderby'    => 'slug'
                ) 
            );  
            $event_type_name = "";
            // Output custom tax (terms)            
            if ( ! empty( $event_types ) && ! is_wp_error( $event_types ) ) {

                foreach ( $event_types as $event_type ) {             
                    $event_type_name = $event_type->name;
                    //$event_type_slug = $event_type->slug;
                }
            }
*/
?>

        <div class="medium-6 large-3 columns">
            <div class="widget widget-profile widget-portrait w-top-image">
                <div class="w-content">
                    <div class="w-body caret-up">
                        <header class="w-header">
                            <h3 class="section-title"><?php the_title(); ?></h3>
                            <h4><?php echo $event_type; ?></h4>
                            <?php if(get_field('start_date')) { ?>
                                <p class="date">
                                    <?php the_field('start_date'); ?><?php if(get_field('end_date')) { ?> - <?php the_field('start_date'); ?><?php } ?>
                            <?php } ?>
                            <?php if(get_field('start_time')) { ?>
                                <br><?php the_field('start_time'); ?><?php if(get_field('end_time')) { ?> - <?php the_field('end_time'); ?><?php } ?>
                            <?php } ?>
                                </p>
                        </header>
                        <div class="w-excerpt">
                            <p><?php the_field('excerpt'); ?></p>
                            <?php if(get_field('speaker')) { ?>
                                <p><strong>Speaker</strong><br>
                                <?php the_field('speaker'); ?></p>
                            <?php } ?>
                            <?php if(get_field('venue')) { ?>
                                <p><strong>Venue</strong><br>
                                <?php the_field('venue'); ?></p>
                            <?php } ?>
                        </div>
                        <?php if(get_field('url')) { ?>
                        <a href="<?php the_field('url'); ?>" class="button button-1">Book Now</a>
                        <?php } ?>
                    </div>
                    <div class="w-image caption">
                        <div class="img" style="background-image: url(<?php echo $image; ?>)"></div>
                    </div>
                </div>
            </div>
        </div>
<?php
    $count++;
	// End: Output posts (foreach)
	}
?>
    </div>
</div>