<?php
// Theme part for Gallery Carousel
//

$gallery_title  = get_field('proj_gallery_title');
$gallery_images = get_field('proj_gallery');
$size = 'Gallery';

if( $gallery_title ): ?>
<div class="section heading">
    <div class="row">
        <div class="large-12 columns">
            <div class="widget-text">
                <h2 class="section-title alone"><?php echo $gallery_title; ?></h2>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php if( $gallery_images ): ?>
<div class="section shaded padded">
    <div class="row">
        <div class="gallery-frame">
            <div class="gallery-carousel default-carousel">

            <?php foreach( $gallery_images as $gallery_image ): ?>

                <div class="slide">
                    <img src="<?php echo $gallery_image['sizes'][$size]; ?>" alt="<?php echo $gallery_image['alt']; ?>" />
                    <?php if( $gallery_image['caption'] ): ?>
                    <div class="gallery-caption">
                        <?php echo $gallery_image['caption']; ?>
                    </div>
                    <?php endif; ?>
                </div>

            <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>
<?php endif; ?>