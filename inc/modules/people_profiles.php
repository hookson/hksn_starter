<?php
// Theme part for People custom post output (1 of 2)

?>
<?php

    $person_query = new WP_Query( array(
        'post_type' => 'person',
        'tax_query' => array(
            array(
          'taxonomy' => 'person_group',
          'terms'    => $person_group_term,
          //'terms'    => 'iv-team',
          'field'    => 'slug',
            )
        ),
        //'meta_key'  => 'ppl_page_order',
        'orderby'   => 'menu_order', //'meta_value_num',
        'order' => 'ASC'
        )
    );
?>
<div class="section tabbed" id="staff">
    <div class="row">
        <div class="large-12 columns">
            <ul class="tabbed-nav<?php echo " ".$person_group_term; ?>">
                <li class="staff_link"><a href="?group=staff#staff">Staff</a></li>
                <li class="board_link"><a href="?group=board-of-trustees#staff">Board of Trustees</a></li>
                <li class="council_link"><a href="?group=advisory-council#staff">Advisory Council</a></p></li>
            </ul>
        </div>
    </div>
</div>
<div class="section shaded padded">
    <div class="row expandable-blocks">
<?php
    $count = 1;
	// Output posts	
	while ( $person_query->have_posts() ) 
        {
            $person_query->the_post();

    		// Prepare content
            if(get_field('ppl_displayname')) {
                $ppl_displayname = get_field('ppl_displayname');
            } else {
                $ppl_displayname = '';
            }

            if($person_group_term=="staff") { 
                $widget_view = "expandable";
            }else{
                $widget_view = "simple";
            }

?>	
        <?php /*<div class="<?php if($widget_view=="expandable") { ?>large-6<?php }else{ ?>large-12<?php } ?> columns"> */?>
        <?php // First Board of Trustees block == full width ?>
        <div class="<?php if($person_group_term=="board-of-trustees" && $count == 1) { ?>large-12<?php }else{ ?>large-6<?php } ?> columns<?php if($widget_view=="expandable") { ?> match-height<?php } ?>">
            <div class="widget-profile<?php if($widget_view=="expandable") { ?> w-expandable<?php }else{ ?> w-simple<?php } ?>" id="<?php echo the_ID(); ?>">
                <div class="w-content">
                    <?php if( has_post_thumbnail() ) { ?>
                    <div class="w-image caption">
                        <?php the_post_thumbnail('thumbnail'); ?>
                    </div>
                    <?php } ?>
                    <div class="w-body">
                        <div class="w-body-inner">
                            <header class="w-header">
                                <h3><?php the_title(); ?></h3>
                                <?php if(get_field('ppl_role')) { ?>
                                    <h4><?php the_field('ppl_role'); ?></h4>
                                <?php } ?>
                            </header>
                            <div class="w-excerpt">
                                <?php the_content(); ?>
                            </div>
                            <?php if(get_field('ppl_email')) { ?>
                            <a tabindex="0" href="mailto:<?php the_field('ppl_email'); ?>" class="button">Contact <?php echo $ppl_displayname; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if($widget_view=="expandable") { ?>
                <span class="more">
                    <span class="button show-more">Show more</span>
                    <span class="button show-less">Show Less</span>
                </span>
                <?php } ?>
            </div>
        </div>
<?php
    $count++;
	// End: Output posts (foreach)
	}
?>
    </div>
</div>