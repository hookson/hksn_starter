<?php
// Theme part for Hero Carousel
//
?>
<div class="hero">
    <div class="hero-carousel default-carousel">

    <?php
        // Output blocks
        while( has_sub_field('crl_slide') ):

        // Prepare content
        $crl_image          = wp_get_attachment_image_src(get_sub_field('crl_image'), 'HeroMain');
        $crl_overlay_image  = wp_get_attachment_image_src(get_sub_field('crl_overlay_image'), 'HeroMain');

    ?>
        <?php /*<div class="slide hero-bg" style="background-image: url(<?php echo $crl_image[0]; ?>);">*/ ?>
        <div class="slide hero-bg" style="background-image: url(<?php echo $crl_image[0]; ?>);">
            <div class="hero-content-wrapper row">
                <div class="hero-content">
                    <h2><?php the_sub_field('crl_headline'); ?></h2>
                    <?php if( get_sub_field( 'crl_button_link') ) { ?><a href="<?php the_sub_field('crl_button_link'); ?>" class="button"><?php the_sub_field('crl_button_label'); ?></a><?php } ?>
                </div>
            </div>
            <?php /*<div class="animated"><?php echo wp_get_attachment_image( $tabcrl_image, $tabcrl_image_size ); ?></div> */ ?>
            <span class="hero-overlay"></span>
        </div>
    <?php
        endwhile;
    ?>
    </div>
</div>