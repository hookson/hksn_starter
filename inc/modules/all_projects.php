<?php
// Theme part for all Projects
?>
<?php
	$args = array(
        'post_type' => 'project',
		//'category' => 6, // News
        'posts_per_page' =>  -1,
    	'orderby' => 'menu_order', // Default
    	'order' => 'ASC', // Default
        'post_status' => 'publish'
	);
	$recent_posts = wp_get_recent_posts( $args );
?>
	<div class="row col-wrapper w-horizontal-cards">
<?php
    $count = 1;
	// Output posts
	foreach( $recent_posts as $recent ){

		// Prepare content
		$post_id = $recent['ID'];
		$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'Teaser' );
		$image = $image_array['0'];

?>
        <div class="medium-6 xlarge-3 columns<?php if($count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">
            <div class="widget widget-portrait default">
                <div class="w-content">
                    <div class="w-image caption">
                        <a href="<?php echo get_permalink($post_id); ?>" class="img"><img src="<?php echo $image; ?>"></a>
                    </div>
                    <div class="w-body">
                        <header class="w-header">
                            <h3><a href="<?php echo get_permalink($post_id); ?>"><?php echo $recent["post_title"]; ?></a></h3>
                            <p class="date"><?php echo date( 'j F Y', strtotime( $recent['post_date'] ) ); ?></p>
                        </header>
                        <div class="w-excerpt">
                            <p><?php echo $recent["post_excerpt"]; ?></p>
                        </div>
                    </div>
                </div>
                <div class="w-footer">
                    <a href="<?php echo get_permalink($post_id); ?>" class="button">Read more</a>
                </div>
            </div>
        </div>
<?php
    $count++;
	// End: Output posts (foreach)
	}
?>
	</div>