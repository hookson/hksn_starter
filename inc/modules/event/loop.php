<?php
// Theme part for events loop (tags.php, archive.php)
?>
    <?php 
      // Prepare content
    $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'Teaser' );
    $image = $image_array['0'];

    if( get_field('event_date') ) {
      $event_date_raw   = get_field('event_date');
      $event_date       = date("jS F Y", strtotime($event_date_raw) );
    }


  /*
      $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'Teaser' );
      $image = $image_array['0'];

      if( get_field('event_start_date') ) {
          $event_start_date_raw   = get_field('event_start_date');
          $event_start_date   = date("F j, Y", strtotime($event_start_date_raw) );
      }

      if( get_field('event_end_date') && ( get_field('event_end_date') != get_field('event_start_date') ) ) {
          $event_end_date_raw   = get_field('event_end_date');
          $event_end_date   = date("F j, Y", strtotime($event_end_date_raw) );

          $event_date = $event_start_date . " - " . $event_end_date;
      }else{
          $event_date = $event_start_date;
      }

      $content = get_the_excerpt();
      $content = apply_filters('the_excerpt', $content);
      $content = str_replace(']]>', ']]&gt;', $content);
      $short_content = wp_trim_words( $content, 26, $more = '… ' );*/

    ?>
                    <div class="medium-12 columns">
                        <div class="widget widget-landscape">
                            <div class="w-content row">
                                <div class="w-image large-6 columns float-right matchheight">
                                    <figure>
                                        <img src="<?php echo $image; ?>" alt="">
                                    </figure>
                                </div>
        
                                <div class="w-body large-6 columns float-left matchheight">
                                    <div class="w-text">
                                        <div class="w-meta"><?php echo $event_date; // 15th November 2016 ?></div>
                                        <h2 class="widget-title"><?php if ( get_field('event_external_link') ) { ?>
                                          <a href="<?php the_field('event_external_link'); ?>"><?php the_title(); ?></a>
                                          <?php } else { ?>
                                            <?php the_title(); ?>
                                          <?php } ?>
                                        </h2>
                                        <h3><?php echo $event_location; ?></h3>
                                        <?php the_field('event_support_copy'); ?>
                                    </div>
                                    <div class="w-footer">
                                        <div class="author-details">&nbsp;</div>
                                        <div class="share-widget">
                                            <button class="default share-btn">Share</button>
        
                                            <div class="share-popup">
                                                <ul>
                                                    <li>
                                                        <a href="" title="Share article on Linkedin"><span class="fa fa-linkedin"></span></a>
                                                    </li>
                                                    <li>
                                                        <a href="" title="Share article on Twitter"><span class="fa fa-twitter"></span></a>
                                                    </li>
                                                    <li>
                                                        <a href="" title="Share article on Facebook"><span class="fa fa-facebook"></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


<?php /*

          <div class="widget-listing cf">
            <div class="large-5 columns">
              <div class="w-image">
                <a href="<?php esc_url( the_permalink() ); ?>" class="img"><img src="<?php echo $image; ?>"></a>
              </div>
            </div>
            <div class="large-7 columns">
              <div class="w-body">
                <h3><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h3>
                <p><?php echo $short_content; ?></p>
                <p class="more"><a href="<?php esc_url( the_permalink() ); ?>">Read more</a></p>
              <?php if(get_field('venue')) { ?>
                <h4><?php the_field('venue'); ?></h4>
              <?php } ?>
                <ul class="w-meta">
                <?php if( $event_date ) { ?>
                  <li><span class="event-date"><?php echo $event_date; ?></span></li>
                <?php } ?>
                <?php if(get_field('event_start_time')) { ?>
                  <li><span class="event-time"><?php the_field('event_start_time'); ?><?php if(get_field('event_end_time')) { ?> - <?php the_field('event_end_time'); ?><?php } ?></span></li>
                <?php } ?>
                  <li class="action"><a href="<?php esc_url( the_permalink() ); ?>" class="button">More details</a></li>
                </ul>
              </div>
            </div>
          </div>*/ ?>