<?php
// Theme part for Hero image block
?>
<?php
  // Get custom fields
  //$hero_image       = wp_get_attachment_image_src( get_field('hero_image'), 'HeroPage' );
  $hero_headline            = get_field('hero_headline');
  $hero_headline_prefix     = get_field('hero_headline_prefix');
  $hero_image               = get_field('hero_image_reference');
  $hero_support_copy        = get_field('hero_support_copy');
  $hero_button_label       = get_field('hero_button_label');
  $hero_button_link        = get_field('hero_button_link');
  //$featured_image   = wp_get_attachment_image_src( get_post_thumbnail_id(), 'HeroPage' );

  if ( is_front_page() ) {
    // Set support copy
    if ( $hero_support_copy ) {
      $hero_support_copy = "<p class='support-copy'>".$hero_support_copy."</p>";
    }
    // Set cta button
    if ( $hero_button_label && $hero_button_link ) {
      $hero_button = "<a href='".$hero_button_link."' class='button primary'>".$hero_button_label."</a>";
    }
  }
?>
        <!-- Hero section -->
<?php if ( $hero_image ) { ?>
        <div class="hero">
            <div class="hero-bg" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/hero/home.jpg);"></div>
            <div class="hero-content-wrapper">
                <div class="row">
                    <div class="xlarge-6 columns hero-content">
                        <h1><?php echo $hero_headline; ?></h1>
                        <?php echo $hero_support_copy; ?>
                        <?php echo $hero_button; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Hero section -->

        <?php /*
<div class="hero">
  <div class="hero-image<?php if ( ! is_front_page() ) { echo " sub-page"; } ?>">

    <div class="slide hero-bg <?php echo $hero_image ?>">
      <div class="frame">

        <div class="hero-content-wrapper row">
          <div class="large-5 large-offset-7 columns">
            <div class="hero-content">
              <h1><?php if( $hero_headline_prefix ) { ?><span><?php echo $hero_headline_prefix; ?> </span><?php } ?> <?php echo $hero_headline; ?></h1>
              <?php echo $hero_support_copy; ?>
              <?php echo $hero_button; ?>
            </div>
          </div>
          <span class="hero-overlay"></span>
        </div>

      </div>
    </div>
<?php if ( ! is_front_page() ) { ?>
    <div class="breadcrumb">
      <div class="row">
        <div class="large-12 columns">
          <?php hksn_breadcrumbs(); ?>
        </div>
      </div>
    </div>
<?php } ?>

  </div>
</div>
<?php */} else { // if ( $hero_image ) { ?>

        <!-- Hero section -->
        <div class="hero small">
            <div class="hero-bg" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/hero/small-hero.jpg);"></div>
        </div>
        <!-- End Hero section -->
        <?php /*
<div class="breadcrumb">
  <div class="row">
    <div class="large-12 columns">
      <?php hksn_breadcrumbs(); ?>
    </div>
  </div>
</div>
<?php */ } // end if ( $hero_image ) {?>
        <!-- End Hero section -->