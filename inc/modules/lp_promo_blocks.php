<?php
// Theme part for Promo blocks (e.g. full width widgets on HP)
?>
<?php    
    $count = 1;
    // Output blocks
    while( has_sub_field('lp_promo_blocks') ):
        // Prepare content
        $lp_promo_headline             = get_sub_field('lp_promo_headline');
        $lp_promo_headline_prefix      = get_sub_field('lp_promo_headline_prefix');
        $lp_promo_support_copy         = get_sub_field('lp_promo_support_copy');
        $lp_promo_button_label         = get_sub_field('lp_promo_button_label');
        $lp_promo_button_link          = get_sub_field('lp_promo_button_link');

        $lp_promo_colour               = get_sub_field('lp_promo_colour'); // light-green orange
        $lp_promo_align                = get_sub_field('lp_promo_align'); // right-aligned

        $lp_promo_image                = get_sub_field('lp_promo_image');

        $lp_promo_image_array = wp_get_attachment_image_src( $lp_promo_image, 'Teaser' );
        $lp_promo_image = $lp_promo_image_array['0'];
?>
<div class="section section-shaded">
    <div class="row">
      <?php if($count % 2 == 0) { ?>
      <div class="widget-landscape promo green image-bg cf" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/becoming-chartered.png');">
      <?php } else { ?>
      <div class="widget-landscape promo light-green cf">
      <?php } ?>
          <div class="large-5 columns">
            <div class="w-image">
              <img src="<?php echo $lp_promo_image; ?>" alt="<?php if( $lp_promo_headline_prefix ) { ?><?php echo $lp_promo_headline_prefix; ?> <?php } ?><?php echo $lp_promo_headline; ?>">
            </div>
          </div>
          <div class="large-6 large-push-1 columns">
            <div class="w-body">
              <h3><?php if( $lp_promo_headline_prefix ) { ?><?php echo $lp_promo_headline_prefix; ?> <?php } ?><span><?php echo $lp_promo_headline; ?></span></h3>   
              <p><?php echo $lp_promo_support_copy; ?></p>
              <?php if( $lp_promo_button_link ) { ?>
              <p><a href="<?php echo $lp_promo_button_link; ?>" class="button"><?php echo $lp_promo_button_label; ?></a></p>
              <?php } ?>
            </div>
          </div>
      </div>
    </div>
</div>

<?php
  $count++; 
	// End: Output blocks (while)
	endwhile;
?>