<?php
// Template:        All Posts Block
// Type:            Parial/Module
// Notes:
// Output all posts (optional exclusion of featured posts)
?>
<?php
    if (isset($_GET['page_no']) && !empty($_GET['page_no'])) {
        $paged = (int)$_GET['page_no'];
    } else {
        $paged = 1;
    }

    $args = array(
        'posts_per_page' => 9,
        'post_status' => 'publish',
        'paged' => $paged
    );
    if (isset($_GET['category_id']) && !empty($_GET['category_id'])) {
        $args['cat'] = (int)$_GET['category_id'];
    } else {
        $args['category__not_in'] = array(709); // NOT in Featured (those in featured_posts.php)
    }

    $query_posts = new WP_Query($args);
?>
<div class="row">
<?php
    if ($query_posts->have_posts()) {
        $count = 1;
        while ($query_posts->have_posts()) {
            $query_posts->the_post();

            // Prepare content
            $post_id = get_the_ID();
            $large_image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'Teaser' );
            $large_image = $large_image_array['0'];
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'TeaserSmall' );
            $image = $image_array['0'];
            //
            $content = get_post_field('post_content', $post_id);
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            $short_content = wp_trim_words( $content, 20, $more = '… ' );
            $long_content = wp_trim_words( $content, 40, $more = '… ' );
?>  
  <div class="large-4 columns">
    <a tabindex="0" href="<?php echo get_permalink($post_id); ?>"><img src="<?php echo $image; ?>"></a>
    <h3><a tabindex="0" href="<?php echo get_permalink($post_id); ?>"><?php the_title(); ?></a></h3>
    <p><?php echo get_the_date( 'Y-m-d' ); ?></p>
    <p><?php echo $short_content; ?></p>
  </div>
<?php
        $count++;
        }
    // End: Output posts (while)
?>
</div>
  <nav class="posts_pagination">
    <?php if ($paged > 1) { ?>
      <a class="button prev-page" data-page="<?php echo $paged; ?>">Newer posts</a>
    <?php } ?>
    <?php if ($paged < $query_posts->max_num_pages) { ?>
      <a class="button next-page" data-page="<?php echo $paged; ?>">Older posts</a>
    <?php } ?>
  </nav>
<?php
    } else {
        echo 'No posts to show. ';
        if ($paged > 1) {
            echo '<a href="'. home_url('/') . '">Go to homepage</a>';
        }
    }
    // clean up after our query
    wp_reset_postdata();
?>