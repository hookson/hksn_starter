<?php
// Template:        Latest Events
// Type:            Parial/Module
// Notes:
// Show latest events
?>
<?php

// Show pagination
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

if ( $events_view == "past") {
  $event_args = array(
    'post_type'   => 'event',
    'posts_per_page' => 10,
    'meta_key'    => 'event_date',
    'orderby'     => 'meta_value_num',
    'order'       => 'ASC',
    'paged'       => $paged,
    'meta_query'  => array(
      array(
        'key'     => 'event_date',
        'type'    => 'NUMERIC', // MySQL needs to treat date meta values as numbers
        'value'   => current_time( 'Ymd' ), // Today in ACF datetime format
        'compare' => '<', // Less than value
      ),
    )
  );

} else {
  $event_args = array(
    'post_type'   => 'event',
    'posts_per_page' => 10,
    'meta_key'    => 'event_date',
    'orderby'     => 'meta_value_num',
    'order'       => 'ASC',
    'paged'       => $paged,
    'meta_query'  => array(
      array(
        'key'     => 'event_date',
        'type'    => 'NUMERIC', // MySQL needs to treat date meta values as numbers
        'value'   => current_time( 'Ymd' ), // Today in ACF datetime format
        'compare' => '>=', // Greater than or equal to value
      ),
    )
  );

}
$event_query = new WP_Query( $event_args ); 
?>
<?php
	/*$args = array(
		'numberposts' => '4',
		//'category' => 58, // Featured
    'orderby' => 'post_date', // Default
    'order' => 'DESC', // Default
    'post_status' => 'publish'
);
$recent_posts = wp_get_recent_posts( $args );*/
?>
<?php if ( $event_query->have_posts() ) : ?>

  <?php 

  $count          = 1;

  // Output events
  while ( $event_query->have_posts() ) : $event_query->the_post();

    // Post query loop           
    include( locate_template( 'inc/modules/event/loop.php' ) ); 

    $count++;
    
  // End: Output events (while)
  endwhile; 
?>

<?php if ($event_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
  <nav class="prev-next-posts">
    <div class="prev-posts-link">
      <?php echo get_next_posts_link( 'Older Entries', $event_query->max_num_pages ); // display older posts link ?>
    </div>
    <div class="next-posts-link">
      <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
    </div>
  </nav>
<?php } ?>

<?php //echo paginate_links( $args ); ?>

<?php wp_reset_postdata(); ?>
<?php else: ?>
    <div class="medium-12 columns">
      <p>No events to display.</p>
    </div>
<?php endif; ?>