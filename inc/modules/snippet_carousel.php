<?php
// Theme part for Snippet carousel (facts, key points etc)
?>
<div class="section shaded padded">
    <div class="row">
        <div class="snippet-carousel default-carousel">

        <?php
            // Output blocks
            while( has_sub_field('snipcrl_slide') ):

            // Prepare content
            //$crl_image = wp_get_attachment_image_src(get_sub_field('crl_image'), 'HeroMain');

            $snipcrl_image1         = get_sub_field('snipcrl_image1');
            $snipcrl_image_size1    = 'full';
            $snipcrl_image2         = get_sub_field('snipcrl_image2');
            $snipcrl_image_size2    = 'full';

            // External links?
            if( get_sub_field('snipcrl_button_extlink1') ) {
                $snipcrl_button_link1 = get_sub_field('snipcrl_button_extlink1');
            }else{
                $snipcrl_button_link1 = get_sub_field('snipcrl_button_link1');
            }
            if( get_sub_field('snipcrl_button_extlink2') ) {
                $snipcrl_button_link2 = get_sub_field('snipcrl_button_extlink2');
            }else{
                $snipcrl_button_link2 = get_sub_field('snipcrl_button_link2');
            }

            // Jump links?
            if( get_sub_field('snipcrl_jump_link1') ) {
                $snipcrl_button_link1 = $snipcrl_button_link1."#".get_sub_field('snipcrl_jump_link1');
            }
            if( get_sub_field('snipcrl_jump_link2') ) {
                $snipcrl_button_link2 = $snipcrl_button_link2."#".get_sub_field('snipcrl_jump_link2');
            }

        ?>
            <div class="row snippet-slide">
                <div class="large-6 columns">
                    <div class="snippet-content">
                        <a href="<?php echo $snipcrl_button_link1; ?>"<?php if( get_sub_field('snipcrl_button_extlink1') ) { echo " target='_blank'"; } ?>><?php echo wp_get_attachment_image( $snipcrl_image1, $snipcrl_image_size1 ); ?></a>
                        <?php /*<h2><?php the_sub_field('snipcrl_headline1'); ?></h2>
                        <?php if( get_sub_field( 'snipcrl_button_link1') ) { ?><a href="<?php the_sub_field('snipcrl_button_link1'); ?>" class="button"><?php the_sub_field('snipcrl_button_label1'); ?></a><?php } ?>*/ ?>
                    </div>
                </div>
                <div class="large-6 columns">
                    <div class="snippet-content">
                        <a href="<?php echo $snipcrl_button_link2; ?>"<?php if( get_sub_field('snipcrl_button_extlink2') ) { echo " target='_blank'"; } ?>><?php echo wp_get_attachment_image( $snipcrl_image2, $snipcrl_image_size2 ); ?></a>
                        <?php /*<h2><?php the_sub_field('snipcrl_headline2'); ?></h2>
                        <?php if( get_sub_field( 'snipcrl_button_link2') ) { ?><a href="<?php the_sub_field('snipcrl_button_link2'); ?>" class="button"><?php the_sub_field('snipcrl_button_label2'); ?></a><?php } ?>*/ ?>
                    </div>
                </div>
            </div>
        <?php
            endwhile;
        ?>
        </div>
    </div>
</div>