<?php
// Theme part for People custom post output (2 of 2)
?>
<?php

    $person_query = new WP_Query( array(
        'post_type' => 'person',
        'tax_query' => array(
            array(
          'taxonomy' => 'group type',
          //'terms'    => 'advisory-board',
          'terms'    => 'iv-team',
          'field'    => 'slug',
            )
        ),
        'meta_key'  => 'ppl_page_order',
        'orderby'   => 'meta_value_num',
        'order' => 'ASC'
        )
    );
?>

<?php
    $count = 1;
	// Output posts	
	while ( $person_query->have_posts() ) 
        {
            $person_query->the_post();

    		// Prepare content
            $person_image = wp_get_attachment_image_src(get_field('featured_image'), 'Profile'); 
?>	
<div class="section light-section profiles<?php if($count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">

    <div class="row">
        <div class="large-12 columns">
            <div class="widget widget-landscape widget-profile">
                <div class="w-content">
                    <div class="w-image left">
                    <?php if( $person_image ) { ?>
                        <img src="<?php echo $person_image[0]; ?>" alt="<?php the_title(); ?>" />
                    <?php } ?>
                    </div>
                    <div class="w-body right">
                        <header class="w-header">
                            <h2 class="section-title"><?php the_title(); ?>, <?php the_field('job_role'); ?></h2>
                        </header>
                    <?php if(get_field('phone')) { ?>
                        <p>Tel. <?php the_field('phone'); ?></p>
                    <?php } ?>
                    <?php if(get_field('mobile')) { ?>
                        <p>Mob. <?php the_field('mobile'); ?></p>
                    <?php } ?>
                        <p><?php the_content(); ?></p>
                        <a href="mailto:<?php the_field('email'); ?>" target="_blank" class="button button-1">
                            <i class="fa fa-email"></i>
                            <span>Contact me</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $count++;
	// End: Output posts (foreach)
	}
?>