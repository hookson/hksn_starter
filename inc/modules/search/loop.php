<?php
// Theme part for search loop (search.php)
?>
    <div class="large-12 columns">
      <div class="widget-portrait">
        <div class="w-body orange">
          <h3><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h3>
          <p><?php the_excerpt(); ?></p>
          <p class="wp-caption-text orange"><?php printf( __( '%s', 'textdomain' ), get_post_type( get_the_ID() ) ); ?></p>
        </div>
      </div>
    </div>