<?php
// Theme part for Promo blocks (e.g. full width widgets on HP)
?>
<?php    
    $count = 1;
    // Output blocks
    while( has_sub_field('promo_blocks') ):
        // Prepare content
        $promo_headline             = get_sub_field('promo_headline');
        $promo_headline_prefix      = get_sub_field('promo_headline_prefix');
        $promo_image                = get_sub_field('promo_image');
        $promo_support_copy         = get_sub_field('promo_support_copy');
        $promo_button_label         = get_sub_field('promo_button_label');
        $promo_button_link          = get_sub_field('promo_button_link');

        $promo_colour               = get_sub_field('promo_colour'); // light-green orange
        $promo_align                = get_sub_field('promo_align'); // right-aligned
?>
<div class="section base-border <?php echo $promo_colour; ?>" id="<?php echo "promo".$count; ?>">  
  <div class="widget-promo fullwidth cf <?php echo $promo_colour; ?> <?php echo $promo_align; ?>">
    <div class="row full-width collapse">
<?php if ( $promo_align == "right-aligned" ) { ?>
      <div class="xlarge-5 xlarge-push-7 large-6 columns">
<?php } else { ?>
      <div class="xlarge-5 large-6 columns">
<?php } ?>
        <div class="w-image arrow-<?php echo $promo_colour; ?>">
          <div class="frame">
            <img src="<?php echo $promo_image; ?>" alt="<?php echo $promo_headline_prefix.$promo_headline; ?>">
          </div>
        </div>
      </div>

<?php if ( $promo_align == "right-aligned" ) { ?>
      <div class="xlarge-7 xlarge-pull-5 large-6 columns">
<?php } else { ?>
      <div class="xlarge-5 large-6 columns">
<?php } ?>
        <div class="w-body <?php echo $promo_colour; ?>">
          <h2><?php if( $promo_headline_prefix ) { ?><span><?php echo $promo_headline_prefix; ?> </span><?php } ?> <?php echo $promo_headline; ?></h2>          
          <div class="w-excerpt">
            <blockquote class="home-blockquote">
              <p><?php echo $promo_support_copy; ?></p>
            </blockquote>
            <div class="btn-wrapper cf">
              <a href="<?php echo $promo_button_link; ?>" class="button"><?php echo $promo_button_label; ?></a>
              <div class="triple-circles <?php echo $promo_align; ?>"><span></span><span></span><span></span></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<?php
  $count++; 
	// End: Output blocks (while)
	endwhile;
?>