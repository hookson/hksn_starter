<?php
// Template:        Page Block (ACF name: 'Content Sections')
// Type:            Parial/Module
// Notes:
// ACF Flexible Content Block used for bulding up pages of content
?>
<?php

// check if the flexible content field has rows of data
if( have_rows('page_block') ):

    $block_count = 1;

     // loop through the rows of data
    while ( have_rows('page_block') ) : the_row();

// ---------------------------------------------------------------------
// Intro Block -----------------------------------------------------------
// Introductory block (title and copy)

        if( get_row_layout() == 'pb_intro_block' ):
?>  
<div id="block_<?php echo $block_count; ?>" class="section <?php the_sub_field('pb_intro_block_class'); if($block_count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">     
    <div class="row">
        <div class="large-12 columns">
            <?php 
                //$block_style = "";
                //$container_style
                /*if( get_sub_field( 'pb_intro_block_content') ) {

                }*/
            ?>
            <h2><?php the_sub_field('pb_intro_block_title'); ?></h2>
            <?php if( get_sub_field( 'pb_intro_block_content') ){ 
                the_sub_field('pb_intro_block_content'); 
            } ?>
        </div>
    </div>
</div>
<?php   
// -----------------------------------------------------------
// end if( have_rows('pb_intro_block') ):
// -----------------------------------------------------------

// ---------------------------------------------------------------------
// Full Width Content Block -----------------------------------------------------------
// Text block (WYSIWYG / article)

        elseif( get_row_layout() == 'pb_content_block' ): 

?>  
<div id="block_<?php echo $block_count; ?>" class="section <?php the_sub_field('pb_content_block_class'); if($block_count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">     
    <div class="row">
        <div class="large-12 columns">
            <?php if( get_sub_field( 'pb_content_block_content') ){ 
                the_sub_field('pb_content_block_content'); 
            } ?>
        </div>
    </div>
</div>
<?php   
// -----------------------------------------------------------
// end if( have_rows('pb_content_block') ):
// -----------------------------------------------------------

// ---------------------------------------------------------------------
// Full Width Promo Block -----------------------------------------------------------
// Promotional block (CTA Banners etc)

        elseif( get_row_layout() == 'pb_fw_block' ): 

            $pb_fw_block_image              = get_sub_field('pb_fw_block_image');
            $pb_fw_block_image_size         = 'Teaser';
            // Get caption for this image (if exists)
            $pb_fw_block_image_attachments  = get_post($pb_fw_block_image);
            $pb_fw_block_image_caption      = $pb_fw_block_image_attachments->post_excerpt;

?>  
<div id="block_<?php echo $block_count; ?>" class="section <?php the_sub_field('pb_fw_block_class'); if($block_count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">     
    <div class="row">
        <div class="large-12 columns">
            <?php echo wp_get_attachment_image( $pb_fw_block_image, $pb_fw_block_image_size ); ?>
            <?php if($pb_fw_block_image_caption){ echo "<span class='w-caption'>".$pb_fw_block_image_caption."</span>"; } ?>     
            <h2><?php the_sub_field('pb_fw_block_title'); ?></h2>
            <?php if( get_sub_field( 'pb_fw_block_content') ){ 
                the_sub_field('pb_fw_block_content'); 
            } ?>
        </div>
    </div>
</div>
<?php   
// -----------------------------------------------------------
// end if( have_rows('pb_fw_block') ):
// -----------------------------------------------------------

// ---------------------------------------------------------------------
// 2-up Widget Block -----------------------------------------------------------
// 2 x Widgets (excerpts, promos etc)

        elseif( get_row_layout() == 'pb_2up_block' ): 

        // Define classes (for section and widget styling)
        $pb_up_block_class = get_sub_field('pb_2up_block_class');
        // default (null) / boxed / 
        // light-bg / dark-bg / pattern-bg
        if (strpos($pb_up_block_class, 'boxed') !== false) {
            $pb_up_widgets_class = " widget-col bordered";
        }elseif (strpos($pb_up_block_class, 'default') !== false) {
            $pb_up_widgets_class = " widget-col bordered";
        }else{
            $pb_up_widgets_class = "";
        }

?>  
<div id="block_<?php echo $block_count; ?>" class="section <?php the_sub_field('pb_2up_block_class'); if($block_count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">     
    <div class="row">

    <?php
    // loop through the rows of data
        while ( have_rows('pb_2up_block_widgets') ) : the_row();

            // Image variables -------------------------------------------------
            $pb_up_block_widgets_image                 = get_sub_field('pb_2up_block_widgets_image');
            $pb_up_block_widgets_image_size            = 'Teaser';
            // Get caption for this image (if exists)
            $pb_up_block_widgets_image_attachments     = get_post($pb_2up_block_widgets_image);
            $pb_up_block_widgets_image_caption         = $pb_2up_block_widgets_image->post_excerpt;
            // Content variables -------------------------------------------------
            $pb_up_block_widgets_title                 = get_sub_field( 'pb_2up_block_widgets_title');
            $pb_up_block_widgets_content               = get_sub_field( 'pb_2up_block_widgets_content'); 
            // If entire widget block is to link -------------------------------------------------
            $pb_up_block_widgets_link                  = get_sub_field('pb_2up_block_widgets_link');
            // If block has 'type' label -------------------------------------------------
            $pb_up_block_widgets_label                 = get_sub_field('pb_2up_block_widgets_label');
            // Widget specific class(es) ---------------------------------------------------------
            $pb_up_block_widgets_class                 = get_sub_field('pb_2up_block_widgets_class');
    ?>
        <div class="large-6 xlarge-6 columns<?php echo $pb_up_widgets_class; ?>">

            <?php if (strpos($pb_up_block_class, 'boxed') !== false) {
                include( locate_template( 'inc/modules/widgets/boxed.php' ) );
            } else {
                include( locate_template( 'inc/modules/widgets/default.php' ) );
            }
            ?>
            <?php /*echo wp_get_attachment_image( $pb_2up_block_widgets_image, $pb_2up_block_widgets_image_size ); ?>
            <?php if($pb_2up_block_widgets_image_caption){ echo "<span class='w-caption'>".$pb_2up_block_widgets_image_caption."</span>"; } ?>     
            <h2><?php the_sub_field('pb_2up_block_widgets_title'); ?></h2>
            <?php if( get_sub_field( 'pb_2up_block_widgets_content') ){ 
                the_sub_field('pb_2up_block_widgets_content'); 
            } */?>
        </div>

    <?php
    // end loop through the rows of data
        endwhile;
    ?>
    </div>
</div>
<?php   
// -----------------------------------------------------------
// end if( have_rows('pb_2up_block') ):
// -----------------------------------------------------------

// ---------------------------------------------------------------------
// 3-up Widget Block -----------------------------------------------------------
// 3 x Widgets (excerpts, promos etc)

        elseif( get_row_layout() == 'pb_3up_block' ): 

?>  
<div id="block_<?php echo $block_count; ?>" class="section <?php the_sub_field('pb_3up_block_class'); if($block_count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">     
    <div class="row">

    <?php
    // loop through the rows of data
        while ( have_rows('pb_3up_block_widgets') ) : the_row();

            // Image variables -------------------------------------------------
            $pb_up_block_widgets_image                 = get_sub_field('pb_3up_block_widgets_image');
            $pb_up_block_widgets_image_size            = 'TeaserSmall';
            // Get caption for this image (if exists)
            $pb_up_block_widgets_image_attachments     = get_post($pb_3up_block_widgets_image);
            $pb_up_block_widgets_image_caption         = $pb_3up_block_widgets_image->post_excerpt;
            // Content variables -------------------------------------------------
            $pb_up_block_widgets_title                 = get_sub_field( 'pb_3up_block_widgets_title');
            $pb_up_block_widgets_content               = get_sub_field( 'pb_3up_block_widgets_content'); 
            // If entire widget block is to link -------------------------------------------------
            $pb_up_block_widgets_link                  = get_sub_field('pb_3up_block_widgets_link');
            // If block has 'type' label -------------------------------------------------
            $pb_up_block_widgets_label                 = get_sub_field('pb_3up_block_widgets_label');
            // Widget specific class(es) ---------------------------------------------------------
            $pb_up_block_widgets_class                 = get_sub_field('pb_3up_block_widgets_class');
    ?>
        <div class="columns large-4">
            <?php include( locate_template( 'inc/modules/widgets/boxed.php' ) ); ?>

            <?php /*echo wp_get_attachment_image( $pb_3up_block_widgets_image, $pb_3up_block_widgets_image_size ); ?>
            <?php if($pb_3up_block_widgets_image_caption){ echo "<span class='w-caption'>".$pb_3up_block_widgets_image_caption."</span>"; } ?>     
            <h2><?php the_sub_field('pb_3up_block_widgets_title'); ?></h2>
            <?php if( get_sub_field( 'pb_3up_block_widgets_content') ){ 
                the_sub_field('pb_3up_block_widgets_content'); 
            } */?>
        </div>

    <?php
    // end loop through the rows of data
        endwhile;
    ?>
    </div>
</div>
<?php   
// -----------------------------------------------------------
// end if( have_rows('pb_3up_block') ):
// -----------------------------------------------------------

// ---------------------------------------------------------------------
// 4-up Widget Block -----------------------------------------------------------
// 4 x Widgets (excerpts, promos etc)

        elseif( get_row_layout() == 'pb_4up_block' ): 

?>  
<div id="block_<?php echo $block_count; ?>" class="section <?php the_sub_field('pb_4up_block_class'); if($block_count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">     
    <div class="row">

    <?php
    // loop through the rows of data
        while ( have_rows('pb_4up_block_widgets') ) : the_row();

            // Image variables -------------------------------------------------
            $pb_up_block_widgets_image                 = get_sub_field('pb_4up_block_widgets_image');
            $pb_up_block_widgets_image_size            = 'TeaserSmall';
            // Get caption for this image (if exists)
            $pb_up_block_widgets_image_attachments     = get_post($pb_4up_block_widgets_image);
            $pb_up_block_widgets_image_caption         = $pb_4up_block_widgets_image->post_excerpt;
            // Content variables -------------------------------------------------
            $pb_up_block_widgets_title                 = get_sub_field( 'pb_4up_block_widgets_title');
            $pb_up_block_widgets_content               = get_sub_field( 'pb_4up_block_widgets_content'); 
            // If entire widget block is to link -------------------------------------------------
            $pb_up_block_widgets_link                  = get_sub_field('pb_4up_block_widgets_link');
            // If block has 'type' label -------------------------------------------------
            $pb_up_block_widgets_label                 = get_sub_field('pb_4up_block_widgets_label');
            // Widget specific class(es) ---------------------------------------------------------
            $pb_up_block_widgets_class                 = get_sub_field('pb_4up_block_widgets_class');
    ?>
        <div class="columns large-3">
            <?php include( locate_template( 'inc/modules/widgets/boxed.php' ) ); ?>

            <?php /*echo wp_get_attachment_image( $pb_4up_block_widgets_image, $pb_4up_block_widgets_image_size ); ?>
            <?php if($pb_4up_block_widgets_image_caption){ echo "<span class='w-caption'>".$pb_4up_block_widgets_image_caption."</span>"; } ?>     
            <h2><?php the_sub_field('pb_4up_block_widgets_title'); ?></h2>
            <?php if( get_sub_field( 'pb_4up_block_widgets_content') ){ 
                the_sub_field('pb_4up_block_widgets_content'); 
            } */?>
        </div>

    <?php
    // end loop through the rows of data
        endwhile;
    ?>
    </div>
</div>
<?php   
// -----------------------------------------------------------
// end if( have_rows('pb_4up_block') ):
// -----------------------------------------------------------

        endif; // get_row_layout() == '...' ):

    $block_count++;

    endwhile; //while ( have_rows('page_block') ) : the_row();

else:

    // no layouts found

endif;

?>