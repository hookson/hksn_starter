<?php
// Template:        Latest Posts
// Type:            Parial/Module
// Notes:
// Show latest posts (e.g. homepage, or footer of other pages)
?>
<?php
if ( !$number_of_posts ) {
  $number_of_posts = 12;
}
if ( $post_category ) {
  $post_args = array(
    'post_type' => 'post',
    'posts_per_page' => $number_of_posts,
    'cat' => $post_category,
    'order_by' => 'date'
  );
} else {
  $post_args = array(
    'post_type' => 'post',
    'posts_per_page' => $number_of_posts,
    'order_by' => 'date'
  );
}
$post_query = new WP_Query( $post_args ); 
?>
<?php if ( $post_query->have_posts() ) : ?>

<?php 
// Add .row wrapper for all but single (full width) posts
if ( $number_of_posts != 1 ) { ?>
<div class="row">
<?php } ?>

  <?php 

  $count          = 1;

  // Output posts
  while ( $post_query->have_posts() ) : $post_query->the_post();

    // Post query loop           
    include( locate_template( 'inc/modules/post/loop.php' ) ); 

    $count++;
    
  // End: Output posts (while)
  endwhile; 
?>
<?php if ( $number_of_posts != 1 && $number_of_posts != 4 ) { ?>
  <?php if ($post_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
    <nav class="prev-next-posts">
      <div class="prev-posts-link">
        <?php echo get_next_posts_link( 'Older Entries', $post_query->max_num_pages ); // display older posts link ?>
      </div>
      <div class="next-posts-link">
        <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
      </div>
    </nav>
  <?php } ?>
<?php } ?>
<?php wp_reset_postdata(); ?>

<?php if ( $number_of_posts != 1 ) { ?>
</div>
<?php } ?>

<?php else: ?>
<div class="row">
    <div class="medium-12 columns">
      <p>No posts to display.</p>
    </div>
</div>
<?php endif; ?>