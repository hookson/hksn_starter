<?php
// Theme part for video content 
// using YouTube oEmbed (video_code) or basic text reference for Vimeo etc (video_reference)

// If oEmbed ACF widget used, extract code
if ( get_field('video_code') ) {

    // get iframe HTML
    $video_code_iframe = get_field('video_code');

    // use preg_match to find iframe src
    preg_match('/src="(.+?)"/', $video_code_iframe, $matches);
    $src = $matches[1];

    // add extra params to iframe src
    $params = array(
        'controls'              => 0,
        'hd'                    => 1,
        'frameborder'           => 0
    );

    $new_src = add_query_arg($params, $src);

    $video_code_iframe = str_replace($src, $new_src, $video_code_iframe);

    // add extra attributes to iframe html
    $attributes = 'frameborder="0"';

    $video_code_iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $video_code_iframe);


} else {

    $video_reference    = get_field('video_reference');
    $video_code_iframe  = '<iframe src="https://player.vimeo.com/video/<?php echo $video_reference; ?>" width="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';

}

    // get other content
    $video_title  = get_field('video_title');
    $video_summary  = get_field('video_summary');
    $video_button_label  = get_field('video_button_label');
    $video_button_link = get_field('video_button_link');
?>
<?php if ($video_title) { ?>
  <h2><?php echo $video_title; ?></h2>
<?php } ?>
<?php echo $video_summary; ?>
<div>
<?php /*<iframe src="https://player.vimeo.com/video/<?php echo $video_reference; ?>" width="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>*/
echo $video_code_iframe;
?>
</div>    
<?php if ($video_button_link) { ?>
  <a href="<?php echo $video_button_link; ?>"><?php echo $video_button_label; ?></a>
<?php } ?>