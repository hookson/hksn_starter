<?php
// Theme part for Tabbed page template

?>
<?php
// Theme part for Resources custom post output
?>
<?php
// -----------------------------------------------------------------------
// Function: define all terms
$getTaxonomyTerms = function($taxonomy, $terms) {
  $result = array();
  if ($taxonomy) {
    $result = array($taxonomy);
  } else {
    if (!empty($terms) && !is_wp_error($terms)) {
      foreach ($terms as $term) {
        array_push($result, $term->slug);
      }
    }
  }
  return $result;
};

// -----------------------------------------------------------------------
// Build all arguments for wp_query
//
// #1: All terms selected (x3)

    if( $cpt1_type || $cpt1_group || $cpt1_location ){
        $custom_post_type1_query = new WP_Query( array(
            'post_type' => 'custom_post_type1',
            'tax_query' => array(

              'relation' => 'AND',

              array(
              'taxonomy' => 'cpt1_type',
              'terms'    => $getTaxonomyTerms($cpt1_type, $cpt1_type_terms),
              'field'    => 'slug',
              ),
              array(
              'taxonomy' => 'cpt1_group',
              'terms'    => $getTaxonomyTerms($cpt1_group, $cpt1_group_terms),
              'field'    => 'slug',
              ),
              array(
              'taxonomy' => 'cpt1_location',
              'terms'    => $getTaxonomyTerms($cpt1_location, $cpt1_location_terms),
              'field'    => 'slug',
              )
            ),
            /*'meta_key'  => 'ev_start_date',
            'orderby'   => 'meta_value_num',
                'order' => 'ASC',
                'meta_query' => array(

                    'relation' => 'OR',

                    array(
                      'key' => 'ev_start_date',
                      'value' => date('Ymd', strtotime('now')),
                      'type' => 'numeric',
                      'compare' => '>=',
                    ),
                    array(
                      'key' => 'ev_end_date',
                      'value' => date('Ymd', strtotime('now')),
                      'type' => 'numeric',
                      'compare' => '>=',
                    ),
                )*/
            )
        );
    } else {
        $custom_post_type1_query = new WP_Query(

            array(
                'post_type' => 'custom_post_type1',
                /*'meta_key'  => 'ev_start_date',
                'orderby'   => 'meta_value_num',
                'order' => 'ASC',
                'meta_query' => array(

                    'relation' => 'OR',

                    array(
                      'key' => 'ev_start_date',
                      'value' => date('Ymd', strtotime('now')),
                      'type' => 'numeric',
                      'compare' => '>=',
                    ),
                    array(
                      'key' => 'ev_end_date',
                      'value' => date('Ymd', strtotime('now')),
                      'type' => 'numeric',
                      'compare' => '>=',
                    ),
                )*/
            )
        );
    }
?>
    <div class="row col-wrapper w-horizontal-cards">
<?php
//echo $event_type;
//echo $event_location_array;
    $count = 1;
  // Output posts
if ( $custom_post_type1_query->have_posts() ) while ( $custom_post_type1_query->have_posts() )
        {
            $custom_post_type1_query->the_post();

            // Prepare content
            $postid = get_the_ID();
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'MenuImage' );
            $image = $image_array['0'];
            $start_date     = get_field('ev_start_date');
            $end_date       = get_field('ev_end_date');
            $button_label   = get_field('button_label');

            if ( !$button_label ) {
                $button_label   = 'Book Now';
            }

            // Get terms for this post
            $cpt1type_terms       = get_the_terms( $postid, 'cpt1_type' );
            $cpt1group_terms      = get_the_terms( $postid, 'cpt1_group' );
            $cpt1location_terms   = get_the_terms( $postid, 'cpt1_location' );

            // Types
            if ( $cpt1type_terms && ! is_wp_error( $cpt1type_terms ) ) {

               $cpt1types = array();

               foreach ( $cpt1type_terms as $cpt1type_term ) {
                  $cpt1types[] = $cpt1type_term->name;
               }

               $cpt1_type_output = join( ", ", $cpt1types );

            }
            // Groups
            if ( $cpt1group_terms && ! is_wp_error( $cpt1group_terms ) ) {

               $cpt1groups = array();

               foreach ( $cpt1group_terms as $cpt1group_term ) {
                  $cpt1groups[] = $cpt1group_term->name;
               }

               $cpt1_group_output = join( ", ", $cpt1groups );

            }
            // Locations
            if ( $cpt1location_terms && ! is_wp_error( $cpt1location_terms ) ) {

               $cpt1locations = array();

               foreach ( $cpt1location_terms as $cpt1location_term ) {
                  $cpt1locations[] = $cpt1location_term->name;
               }

               $cpt1_location_output = join( ", ", $cpt1locations );

            }



?>
        <div class="medium-6 xlarge-3 columns<?php if($count % 2 == 0) { echo " even"; } else { echo " odd"; } ?>">
            <div class="widget widget-portrait default">
                <div class="w-content">
                    <?php if( $image ){ ?> 
                    <div class="w-image caption">
                        <a href="<?php echo get_permalink($post_id); ?>" class="img"><img src="<?php echo $image; ?>"></a>
                    </div>
                    <?php } ?> 
                    <div class="w-body">
                        <header class="w-header">
                            <h3><a href="<?php echo get_permalink($post_id); ?>"><?php the_title(); ?></a></h3>
                        </header>
                        <div class="w-excerpt">
                            <p><?php the_field('excerpt'); ?></p>
                            <p><strong>Types:</strong><br><?php echo $cpt1_type_output; ?></p>
                            <p><strong>Groups:</strong><br><?php echo $cpt1_group_output; ?></p>
                            <p><strong>Locations:</strong><br><?php echo $cpt1_location_output; ?></p>
                        </div>
                    </div>
                </div>
                <div class="w-footer">
                        <?php if(get_field('url')) { ?>
                        <a href="<?php the_field('url'); ?>" class="button"><?php echo $button_label; ?></a>
                        <?php }else{ ?>
                    <a href="<?php echo get_permalink($post_id); ?>" class="button">Read more</a>
                        <?php } ?>
                </div>
            </div>
        </div>
<?php
    $count++;
  // End: Output posts (foreach)

  }else{
?>
        <div class="widget-text">
            <div class="w-header">
              <h1>No posts found</h1>
            </div>
            <hr class="divider-half">
            <p>There are currently no posts that match your selection(s). Please check back soon or <a href="<?php the_permalink(); ?>">view all posts</a>.</p>
        </div>
<?php
  }
?>
    </div>