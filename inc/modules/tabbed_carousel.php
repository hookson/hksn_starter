<?php
// Theme part for Tabbed Carousel
//
$type = "full-width"; // Set this as full width (like hero carousel)
?>
<?php
// ---------------------------------------------------------------------
// Intro Block -----------------------------------------------------------
// Does this carousel have a title and/or summary?
if( get_field( 'tabcrl_title') ): 

?>
<div class="section heading">
    <div class="row">
        <div class="large-12 columns">
            <div class="widget-text">
                <h2 class="section-title"><?php the_field('tabcrl_title'); ?></h2>
                <?php if( get_field( 'tabcrl_summary') ){ the_field('tabcrl_summary'); } ?>
            </div>
        </div>
    </div>
</div>
<?php        
    endif;
?>
<div class="section">

        <div class="tabbed-carousel default-carousel">

        <?php
            // Output blocks
            while( has_sub_field('tabcrl_slide') ):

            // Prepare content
            $tabcrl_image           = get_sub_field('tabcrl_image');
            $tabcrl_image_size      = 'Gallery';
            $tabcrl_bgimage         = wp_get_attachment_image_src(get_sub_field('tabcrl_image'), 'HeroMain');

        ?>
        <?php if( $type == "full-width" ){ ?>
            <div class="slide hero-bg" style="background-image: url(<?php echo $tabcrl_bgimage[0]; ?>);" data-tab="<?php the_sub_field('tabcrl_headline'); ?>">
        <?php } else { ?>
            <div class="slide" data-tab="<?php the_sub_field('tabcrl_headline'); ?>">
        <?php } ?>
                <div class="tabbed-caption">
                <?php if( get_sub_field( 'tabcrl_button_link') ) { ?>
                    <h3><a href="<?php the_sub_field('tabcrl_button_link'); ?>"><?php the_sub_field('tabcrl_headline'); ?></a></h3>
                <?php } else { ?>
                    <h3><?php the_sub_field('tabcrl_headline'); ?></h3>
                <?php } ?>
                    <?php if( get_sub_field( 'tabcrl_copy') ) { ?><p><?php the_sub_field('tabcrl_copy'); ?></p><?php } ?>
                    <?php if( get_sub_field( 'tabcrl_button_link') ) { ?><p><a href="<?php the_sub_field('tabcrl_button_link'); ?>" class="caption-link"><?php the_sub_field('tabcrl_button_label'); ?> <span>&raquo;</span></a><p><?php } ?>
                </div>
                <?php if( $type != "full-width" ){ ?>
                    <?php echo wp_get_attachment_image( $tabcrl_image, $tabcrl_image_size ); ?>
                <?php } ?>
            </div>
        <?php
            endwhile;
        ?>
        </div>

</div>