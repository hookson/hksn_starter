<?php
// Theme part for Logo Gallery (Cyberhawk)

$lgallery_images = get_field('lgallery_gallery');
$size = 'thumbnail';
?>
<div class="section bb">
    <div class="row">
        <div class="large-12 columns">
            <div class="widget-text">
                <h2 class="section-title"><?php the_field('lgallery_title'); ?></h1>
                <?php the_field('lgallery_support_copy'); ?>
            </div>
        </div>
    </div>
</div>
<?php if( $lgallery_images ): ?>
<!--
// Start: Centering Carousel
-->
<?php 
    $count = 0;
    // Output posts
?>
<div class="section light-section client-list">
    <div class="row">
        <div class="default-carousel center-carousel">
                <?php foreach( $lgallery_images as $lgallery_image ): ?>  

                <?php if($count == 0) { ?>
            <div>
                <div class="column columns">
                    <div class="snippet-content">
                        <div class="snippet-img">
                            <img src="<?php echo $lgallery_image['sizes'][$size]; ?>" alt="<?php echo $lgallery_image['alt']; ?>" />
                        </div> 
                <?php } else { ?> 
                        <div class="snippet-img">
                            <img src="<?php echo $lgallery_image['sizes'][$size]; ?>" alt="<?php echo $lgallery_image['alt']; ?>" />
                        </div> 
                <?php } ?> 
                
                <?php if($count % 3 == 0 && $count != 0) { ?>
                  </div>
                </div>
            </div>
            <div>
                <div class="column columns">
                    <div class="snippet-content">
                <?php } ?> 
                <?php 

                $count++;

                endforeach; 

                ?>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>