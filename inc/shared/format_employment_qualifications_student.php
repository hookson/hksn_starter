<?php
// Theme part for Application forms: Qualifications STUDENTS only
//
?>
<option>University of Aberdeen BSc (Hons) Forestry (8 points)</option> 
<option>University of Aberdeen BSc (Hons) Forest Sciences (8 points)</option> 
<option>University of Aberdeen BSc (Ordinary) Forestry (7 points)</option> 
<option>University of Aberdeen BSc (Ordinary) Forest Sciences (7 points)</option> 
<option>University of Aberdeen MSc Forestry (6 points)</option> 
<option>University of Aberdeen PGDip Forestry (5 points)</option> 
<option>Bangor University BSc (Hons) Forestry (8 points)</option> 
<option>Bangor University BSc (Hons) Forestry and Environmental Management (7 points)</option> 
<option>Bangor University BSc (Hons) Conservation with Forestry (6 points)</option> 
<option>Bangor University Mfor Forestry (8 points)</option> 
<option>Bangor University MSc Environmental Forestry (6 points)</option> 
<option>Bangor University MSc Forestry (6/5 points Depending on modules chosen)</option> 
<option>Bangor University MSc Tropical Forestry (5 points)</option> 
<option>Bangor University MSc Agroforestry (5 points)</option> 
<option>Harper Adams University MSc Forestry Management (6 points)</option> 
<option>Harper Adams University MSc Conservation and Forest Protection (6 points)</option> 
<option>Harper Adams University BSc (Hons) Countryside Management (5 points)</option> 
<option>Harper Adams University BSc (Hons) Countryside and Environment Management (5 points)</option> 
<option>Moulton College (University of Northampton) FdSc Arboriculture (5 points)</option> 
<option>Myerscough College (University of Central Lancashire) BSc (Hons) Arboriculture &amp; Urban Forestry (8 points)</option> 
<option>Myerscough College (University of Central Lancashire) MSc Arboriculture &amp; Urban Forestry (6 points)</option> 
<option>Myerscough College (University of Central Lancashire) FdSc Arboriculture (5 points)</option> 
<option>National School of Forestry (University of Cumbria) BSc (Hons) Forest &amp; Woodland Management (8 points)</option> 
<option>National School of Forestry (University of Cumbria) BSc (Hons) Forest Management (8 points)</option> 
<option>National School of Forestry (University of Cumbria) MSc Forest Ecosystem Management (6 points)</option> 
<option>National School of Forestry (University of Cumbria) BSc (Hons) Woodland Ecology and Conservation (6 points)</option> 
<option>National School of Forestry (University of Cumbria) BSc (Hons) Forestry and Woodland Conservation (5 points)</option> 
<option>National School of Forestry (University of Cumbria) FdSc Forestry (5 points)</option> 
<option>Pershore College (Worcester University) BSc (Hons) Arboriculture and Tree Management (8 points)</option> 
<option>Pershore College (Worcester University) FdSc Arboriculture (5 points)</option> 
<option>Plumpton College (University of Brighton) FdSc Arboriculture (5 points) Prior to October 2013 - (7 points)</option> 
<option>Plumpton College (University of Brighton) FdSc Forestry &amp; Woodland Management (5 points) Prior to October 2013 - (7 points)</option> 
<option>Scottish School of Forestry (Inverness College, UHI) BSc (Ordinary points) Sustainable Forest Management (7 points) With Forest Conservation</option> 
<option>Scottish School of Forestry (Inverness College, UHI) BSc (Ordinary points) Sustainable Forest Management (7 points) With Arboriculture &amp; Urban Forestry</option> 
<option>Scottish School of Forestry (Inverness College, UHI) HND Forestry (5 points)</option> 
<option>Scottish School of Forestry (Inverness College, UHI) HND Arboriculture &amp; Urban Forestry (5 points)</option> 
<option>Sparsholt College (University of Portsmouth) BSc (Hons) Woodland Conservation Management (6 points)</option> 
<option>Sparsholt College (University of Portsmouth) FdSc Woodland Conservation Management (5 points)</option> 
<option value="Other">OTHER (Please provide details in the box provided)</option>