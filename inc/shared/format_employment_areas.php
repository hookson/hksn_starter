<?php
// Theme part for Application forms: Current employment categories 
//
?>
<option>Arboriculture</option>
<option>Biodiversity and wildlife management</option>
<option>Forest engineering</option>
<option>Forestry and arboricultural research</option>
<option>Forestry and woodland management</option>
<option>Harvesting, marketing and utilisation</option>
<option>Inventory and mapping</option>
<option>Land use appraisals, environmental assessments and landscape design</option>
<option>Litigation in connection with trees and timber</option>
<option>Management &amp; administration</option>
<option>Public policy</option>
<option>Recreation management and planning</option>
<option>Timber processing</option>
<option>Training and education</option>
<option>Tree nursery management</option>
<option>Tree surveys and reports</option>
<option>Urban forestry</option>
<option>Utility arboriculture</option>
<option>Woodland investment and valuations</option>