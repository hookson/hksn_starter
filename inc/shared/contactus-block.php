<!-- \\ Newsletter sign-up -->
<div class="section newsletter">
  <div class="row">
    <div class="columns large-8">
      <h3><span></span>Contact Us</h3>
    </div>
    <div class="columns large-4">
      <p>Find out more about ICF membership and other services by contacting us.</p>
      <a href="<?php echo home_url('/'); ?>contact-us/" class="button">Contact Now</a>
    </div>
    <div class="triple-circles newsletter-section white">
        <span></span>
        <span></span>
        <span></span>
    </div>
  </div>
</div>
<!-- // Newsletter sign-up -->
