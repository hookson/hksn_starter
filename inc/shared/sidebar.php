
	<aside class="sidebar">

	<?php // Extra Content (ACF)
		if( get_field('sb-info_copy') ): ?>
        <div class="widget widget-text">
          <div class="widget-container">
            <h5><?php the_field('sb-info_title'); ?></h5>
            <?php the_field('sb-info_copy'); ?>
          </div>
        </div>
    <?php endif ?>

	<?php // Related Content (ACF)
		if( get_field('sb-rel_list') ): ?>
        <div class="widget widget-list">
          <div class="widget-container">
            <h5><?php the_field('sb-rel_title'); ?></h5>

            <?php while( has_sub_field('sb-rel_list') ): ?>
            <ul class="wl-blocklist">
              <li><a href="<?php the_sub_field('sb-rel_url'); ?>"><?php the_sub_field('sb-rel_label'); ?></a></li>
            </ul>
            <?php endwhile; ?>            
          </div>
        </div>
    <?php endif ?>

    </aside>