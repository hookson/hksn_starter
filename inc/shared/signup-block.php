<!-- \\ Newsletter sign-up -->
<div class="section newsletter">
  <div class="row">
    <div class="columns large-8">
      <h3><span>Subscribe to our</span>Newsletter</h3>
    </div>
    <div class="columns large-4">
      <p>Get the latest news from the forestry and arboricultural sector, straight to your inbox.</p>
      <a href="<?php echo home_url('/'); ?>subscribe-to-our-newsletter/" class="button">Subscribe Now</a>
    </div>
    <div class="triple-circles newsletter-section white">
        <span></span>
        <span></span>
        <span></span>
    </div>
  </div>
</div>
<!-- // Newsletter sign-up -->
