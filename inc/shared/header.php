<?php
/**
 * The header template
**/
?>
<header>
<h1><a href="<?php echo home_url('/'); ?>"></a></h1> 
<ul>  
  <?php
    // Primary Menu/Nav
    // Outputs minus container ul
    hksn_primary_nav(); 
  ?> 
</ul>
<?php get_search_form( ); ?>
<!-- End: top-bar -->
</header>