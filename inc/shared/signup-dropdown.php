<div class="signup-bar">
  <!-- Begin MailChimp Signup Form -->
    <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate data-abide>
      <div id="mc_embed_signup_scroll" class="row collapse">

        <div class="large-7 columns">
          <h4 class="form-title">Sign up to receive occasional updates from The Company</h4>
        </div>
        <div class="mc-field-group small-10 large-4 columns">
          <input type="email" value="" placeholder="Enter Email Address" name="EMAIL" class="required email" id="mce-EMAIL" required>
        </div>

          <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
          </div>       
          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          <div style="position: absolute; left: -5000px;"><input type="text" name="b_e1c56b4b541bc50fd294eab1a_55f9ca1fc3" tabindex="-1" value=""></div>

        <div class="small-2 large-1 columns">
          <button id="mc-embedded-subscribe"><i class="fa fa-chevron-right"></i><span>Subscribe</span></button>
        </div>
      </div>
    </form>
  <!--End mc_embed_signup-->
  <i class="fa fa-close" id="signup-bar--close"></i>
</div>
