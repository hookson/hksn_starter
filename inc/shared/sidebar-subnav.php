      <aside class="sidebar"> 
        <div id="sub-nav">
          <h5 class="subnav-title">In this section</h5>
          <ul class="subnav-group">
            <li>
              <?php if ( $post->post_parent ) { ?>
              <h4 class="subnav-group-title"><a href="<?php echo get_permalink( $post->post_parent ); ?>" >
                  <?php echo get_the_title( $post->post_parent ); ?>
               </a>
              <?php } ?></h4>
              <?php
              $output = wp_list_pages( array(
                  'echo'     => 0,
                  'depth'    => 1,
                  'title_li' => '<h2>' . __( 'Top Level Pages', 'textdomain' ) . '</h2>'
              ) );
               
              if ( is_page() ) {
                
                $page = $post->ID;
                if ( $post->post_parent ) {
                  $page = $post->post_parent;
                }
               
                $children = wp_list_pages( array(
                      'echo'     => 0,
                      'child_of' => $page,
                      'title_li' => ''
                  ) );
               
                  if ( $children ) {
                      $output = wp_list_pages( array(
                          'echo' => 0,
                          'child_of' => $page,
                          'title_li' => ''
                      ) );
                      $output = "<ul>" . $output . "</ul>";
                  }
              }
              echo $output;
?>
            </li>
          </ul>
        </div>

      </aside>