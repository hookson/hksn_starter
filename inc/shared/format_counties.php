<?php
// Theme part for Application forms: Counties
//
?>
<optgroup label="England">
    <option>Bedfordshire</option>
    <option>Berkshire</option>
    <option>Buckinghamshire</option>
    <option>Cambridgeshire</option>
    <option>Cheshire</option>
    <option>Cleveland</option>
    <option>Cornwall</option>
    <option>County Durham</option>
    <option>Cumbria</option>
    <option>Derbyshire</option>
    <option>Devon</option>
    <option>Dorset</option>
    <option>East Riding of Yorkshire</option>
    <option>East Sussex</option>
    <option>Essex</option>
    <option>Gloucestershire</option>
    <option>Greater London</option>
    <option>Greater Manchester</option>
    <option>Hampshire</option>
    <option>Herefordshire</option>
    <option>Hertfordshire</option>
    <option>Isle of Wight</option>
    <option>Isles of Scilly</option>
    <option>Kent</option>
    <option>Lancashire</option>
    <option>Leicestershire</option>
    <option>Lincolnshire</option>
    <option>Merseyside</option>
    <option>Norfolk</option>
    <option>North Yorkshire</option>
    <option>Northamptonshire</option>
    <option>Northumberland</option>
    <option>Nottinghamshire</option>
    <option>Oxfordshire</option>
    <option>Rutland</option>
    <option>Shropshire</option>
    <option>Somerset</option>
    <option>South Yorkshire</option>
    <option>Staffordshire</option>
    <option>Suffolk</option>
    <option>Surrey</option>
    <option>Tyne &amp; Wear</option>
    <option>Warwickshire</option>
    <option>West Midlands</option>
    <option>West Sussex</option>
    <option>West Yorkshire</option>
    <option>Wiltshire</option>
    <option>Worcestershire</option>
</optgroup>

<optgroup label="Northern Ireland">
    <option>Antrim</option>
    <option>Armagh</option>
    <option>Down</option>
    <option>Fermanagh</option>
    <option>Londonderry</option>
    <option>Tyrone</option>
</optgroup>

<optgroup label="Scotland">
    <option>Aberdeenshire</option>
    <option>Angus</option>
    <option>Argyll &amp; Bute</option>
    <option>Ayrshire</option>
    <option>Banffshire</option>
    <option>Berwickshire</option>
    <option>Borders</option>
    <option>Caithness</option>
    <option>Clackmannanshire</option>
    <option>Dumfries &amp; Galloway</option>
    <option>Dunbartonshire</option>
    <option>East Ayrshire</option>
    <option>East Dunbartonshire</option>
    <option>East Lothian</option>
    <option>East Renfrewshire</option>
    <option>Fife</option>
    <option>Highland</option>
    <option>Inverclyde</option>
    <option>Kincardineshire</option>
    <option>Lanarkshire</option>
    <option>Midlothian</option>
    <option>Moray</option>
    <option>North Ayrshire</option>
    <option>North Lanarkshire</option>
    <option>Orkney</option>
    <option>Perth &amp; Kinross</option>
    <option>Renfrewshire</option>
    <option>Shetland</option>
    <option>South Ayrshire</option>
    <option>South Lanarkshire</option>
    <option>Stirlingshire</option>
    <option>West Dunbartonshire</option>
    <option>West Lothian</option>
    <option>Western Isles</option>
</optgroup>

<optgroup label="Wales">
    <option>Blaenau Gwent</option>
    <option>Bridgend</option>
    <option>Caerphilly</option>
    <option>Cardiff</option>
    <option>Carmarthenshire</option>
    <option>Ceredigion</option>
    <option>Conwy</option>
    <option>Denbighshire</option>
    <option>Flintshire</option>
    <option>Gwynedd</option>
    <option>Isle of Anglesey</option>
    <option>Merthyr Tydfil</option>
    <option>Monmouthshire</option>
    <option>Neath Port Talbot</option>
    <option>Newport</option>
    <option>Pembrokeshire</option>
    <option>Powys</option>
    <option>Rhondda Cynon Taff</option>
    <option>Swansea</option>
    <option>Torfaen</option>
    <option>Vale of Glamorgan</option>
    <option>Wrexham</option>
</optgroup>