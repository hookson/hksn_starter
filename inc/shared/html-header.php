<!doctype html>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]-->
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
	<head>
		<title><?php wp_title( '' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php // Fix Validation error with <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				if (isset($_SERVER['HTTP_USER_AGENT']) &&
				    (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
				        header('X-UA-Compatible: IE=edge,chrome=1');
			  	?>
		<!-- Icons -->
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
		<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#09231c">
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico">
		<meta name="msapplication-config" content="favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">


  		<link href='http://fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i|Open+Sans:300,400,600,700,300italic,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <!--[if IE 8]>
		    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie8.css" />
		  <![endif]-->

		<!-- AddEvent -->
		<script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>
		<!-- AddEvent Settings -->
		<script type="text/javascript">
		window.addeventasync = function(){
		    addeventatc.settings({
		        license    : "anw30hg3qzm03vqm0mcd754",
		        css        : false,
		        outlook    : {show:true, text:"Outlook"},
		        google     : {show:true, text:"Google <em>(online)</em>"},
		        yahoo      : {show:true, text:"Yahoo <em>(online)</em>"},
		        outlookcom : {show:true, text:"Outlook.com <em>(online)</em>"},
		        appleical  : {show:true, text:"Apple Calendar"},
		        facebook   : {show:true, text:"Facebook Event"},
		        dropdown   : {order:"outlook,google,appleical"}
		    });
		};
		</script> 

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
