<!--
// Start: Register Online
-->
<div class="section section-white register-online">
  <div class="row">
    <div class="columns large-8 large-pull-1 register-images">
      <div class="image-circle start">
        <img src="<?php echo get_template_directory_uri(); ?>/img/register-online_img1.jpg" alt="">
      </div>
      <div class="image-circle">
        <img src="<?php echo get_template_directory_uri(); ?>/img/register-online_img2.jpg" alt="">
      </div>
      <div class="image-circle end">
        <img src="<?php echo get_template_directory_uri(); ?>/img/register-online_img3.jpg" alt="">
      </div>
    </div>
    <div class="columns large-4">
      <h3><span>Apply</span> Online</h3>
      <p>Start your journey to chartered status by joining ICF with our online application form.</p>
      <a href="<?php echo home_url('/'); ?>/join-us/register/" class="button">Join Now</a>
    </div>
    <div class="triple-circles newsletter-section orange">
        <span></span>
        <span></span>
        <span></span>
    </div>
  </div>
</div>
<!--
// End: Register Online
-->
