<footer>
<!-- \\ Site info -->
<h4>Footer Links 1</h4>
<?php hksn_footer_links1(); ?>
<h4>Footer Links 2</h4>
<?php hksn_footer_links2(); ?>
<h4>Footer Links 3</h4>
<?php hksn_footer_links3(); ?>
<h4>Footer Links 4</h4>
<?php hksn_footer_links4(); ?>
<!-- // Site info -->
<!-- \\ Site support -->
    <p>&copy; <?php echo date('Y'); ?> Company. All Rights Reserved</p>
    <ul>
      <li><a href="<?php echo home_url('/'); ?>">Privacy Policy</a></li>
      <li><a href="<?php echo home_url('/'); ?>">Sitemap</a></li>
      <li><a href="<?php echo home_url('/'); ?>">Terms &amp; Conditions</a></li>
    </ul>
    <a href="http://www.hookson.com" target="_blank" title="Go to Hookson Branding and Digital Agency Edinburgh website - opens in new tab" class="site-credit site-credit-icon">site: hookson</a>
<!-- // Site support -->
</footer>