<?php
// Theme part for Application forms: Current employment categories 
//
?>
<option>Forestry Commission GB</option>
<option>Forestry Commission England</option>
<option>Forestry Commission Scotland</option>
<option>Natural Resources Wales</option>
<option>Forestry department non-GB</option>
<option>Forest Research</option>
<option>Other research organisation</option>
<option>Central government</option>
<option>Large woodland/arboriculture company</option> 
<option>Small woodland/arboriculture company</option>
<option>Local Authority</option>
<option>Educational Institute</option>
<option>Private estate</option>
<option>Property management</option>
<option>Woodland owner</option>
<option>Timber processing</option>
<option>Nursery</option>
<option>Large corporate</option>
<option>Small corporate</option>
<option>NGO</option>
<option>Retired</option>
<option>Self-employed consultant</option>
<option>Self-employed contractor</option>
<option value="Other">Other (Please specify in the field provided)</option>