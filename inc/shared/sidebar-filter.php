<?php
// Template Name:   Post Filter Block
// Type:            Parial/Module
// Notes:
// Filter and tag cloud for posts (optional exclusion of featured posts)
?>
<?php $args = array(
   // 'smallest' => 8, // size of least used tag
   // 'largest'  => 22, // size of most used tag
   // 'unit'     => 'px', // unit for sizing the tags
   'number'   => 45, // displays at most 45 tags
   'orderby'  => 'name', // order tags alphabetically
   'order'    => 'ASC', // order tags by ascending order
   'taxonomy' => 'post_tag' // you can even make tags for custom taxonomies
);

  $categories = get_categories(array(
    'type'    => 'post',
    'exclude' => array(709), // NOT in Featured (those in featured_posts.php)
  ));

  if (isset($_GET['category_id']) && !empty($_GET['category_id'])) {
      $selected_category_id = (int)$_GET['category_id'];
  } else {
      $selected_category_id = null;
  }

?>
<h5 class="subnav-title">Filter news &amp; blogs</h5>
<select class="filter-by-category">
  <option value="all">All categories</option>
  <?php foreach ($categories as $category) { ?>
    <option value="<?php echo $category->term_id; ?>"<?php echo ($selected_category_id == $category->term_id) ? 'selected' : ''; ?>><?php echo $category->name; ?></option>
  <?php } ?>
</select>
<div class="tag-cloud">
  <?php wp_tag_cloud( $args ); ?>
</div>